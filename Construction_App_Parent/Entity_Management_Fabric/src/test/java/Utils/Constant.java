
package Utils;

public class Constant {


	public static final String OperationLocation = "INSPECTIONFORMS_CREATION";
	public static final String CompanyName = "Automation_Creation_Company";
	public static final String Website = "Visur.com";
	public static final String NoOfEmployess = "50";
	
	public static final String UpdateCompanyName = "Update_Automation_Creation_Company";
	public static final String UpdateWebsite = "Visurtechnology.com";
	public static final String UpdateNoOfEmployess = "60";
	
	public static final String MainPhone = "7485963210";
	public static final String contactmail = "automationvms@gmail.com";
	public static final String FirstName = "QA Automation";
	public static final String LastName = "Person";
	public static final String timezone = "Asia/Kol";


}

