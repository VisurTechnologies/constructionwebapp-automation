package People_CRUD_Scripts;
import org.testng.annotations.Test;
import EntityManagement_POM.EntityManagement_Objects;
import People_Objects.People_Common_Objects;
import Utils.Constant;
import Utils.LoginSetup;
public class Person_Creation extends LoginSetup{
	@Test(groups = {"Sanity"})
	public void person_Creation() throws Exception {
		System.out.println("People_Creation----Execution Started");
		
		EntityManagement_Objects entityManagement_Objects=new EntityManagement_Objects(driver);
		People_Common_Objects people_Common_Objects =new People_Common_Objects(driver);
		
			
		entityManagement_Objects.clickOnLeftTree_PeoplAndCompanies();
		people_Common_Objects.ClickPlusButton();
		people_Common_Objects.ClickEntityType();
		people_Common_Objects.selectPersonEntityType();
		people_Common_Objects.firstname(Constant.FirstName);
		people_Common_Objects.lastname(Constant.LastName);
		people_Common_Objects.Company(Constant.CompanyName);
		people_Common_Objects.Email(Constant.contactmail);
		people_Common_Objects.Mobile(Constant.MainPhone);
		people_Common_Objects.Time_Zone(Constant.timezone);
		people_Common_Objects.SaveButton();
		
		System.out.println("People_Creation----Execution Sucessfull");

	}

}
