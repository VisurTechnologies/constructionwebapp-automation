package log4j2_Implementation;

import javax.servlet.ServletContextEvent;

import org.apache.log4j.xml.DOMConfigurator;

public class Log4jInitListener {
	public void contextDestroyed(ServletContextEvent paramServletContextEvent)  { 
		
    }
 
    public void contextInitialized(ServletContextEvent servletContext)  { 
        String webAppPath = servletContext.getServletContext().getRealPath("/");
        String log4jFilePath = webAppPath + "WEB-INF/classes/log4j.xml";
        DOMConfigurator.configure(log4jFilePath);
        System.out.println("initialized log4j configuration from file:"+log4jFilePath);
    }
}
