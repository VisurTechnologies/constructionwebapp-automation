package Procedures_Object;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Utils.BaseDriver;
import Utils.WebDriverCommonLib;

public class Procedure_attached_images_objects extends BaseDriver{

	public Procedure_attached_images_objects(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	
	Actions actions = new Actions(driver);
	WebDriverWait wait = new WebDriverWait(driver, 2);
	WebDriverCommonLib wdc = new WebDriverCommonLib(driver);
	JavascriptExecutor js = (JavascriptExecutor) driver; 
	
	public By Procedureattached_imageExpandButton = By.xpath("//*[text()='PROCEDURE ATTACHED IMAGES']/ancestor::div[2]//*[@class='exp-icon toggle-icon']");
	public By Procedures_attached_imageAddButton = By.xpath("//*[text()='PROCEDURE ATTACHED IMAGES']/ancestor::div[2]//*[contains(@name,'plus')]");
	public By BrowseImages = By.xpath("//*[text()='browse Image']");
	public By Uploadedimage = By.xpath("//*[@ng-reflect-message='Automation_image.png']");
	public By DeleteImage = By.xpath("//*[@name='delete']");
	public By CheckImagename = By.xpath("//*[@name='imagename']");
	public By checkImageDate = By.xpath("//*[@name='imagedate']");
	
	
	public void CheckImageDate() {
		wdc.waitForExpactedElement_150(driver.findElement(checkImageDate));
		if(driver.findElement(checkImageDate).isDisplayed()) {
			System.out.println("Image Date is displayed");
		}else {
			System.out.println("Image Date is not displayed");
		}
	}
	
	
	public void CheckImageName() {
		wdc.waitForExpactedElement_150(driver.findElement(CheckImagename));
		if(driver.findElement(CheckImagename).isDisplayed()) {
			System.out.println("Image name is displayed");
		}else {
			System.out.println("Image Name is not displayed");
		}
	} 
	public void clickOnProcedure_Attached_image_Addbutton() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(Procedures_attached_imageAddButton));
		actions.click(driver.findElement(Procedures_attached_imageAddButton)).build().perform();
		System.out.println("System clicked On Procedures_attached_image AddButton");
		wdc.waitForPageLoad();
			
	}
	
	public void clickOnProcedure_Attached_image_EXPAND() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(Procedureattached_imageExpandButton));
		actions.click(driver.findElement(Procedureattached_imageExpandButton)).build().perform();
		System.out.println("System clicked On Procedureattached_imageExpandButton");
		wdc.waitForPageLoad();
			
	}
	
	public void UploadImage() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(BrowseImages));
		actions.click(driver.findElement(BrowseImages)).build().perform();
		System.out.println("System clicked On BrowseImages AddButton");
		wdc.waitForPageLoad();
		Robot robot = new Robot();
	     
        // copying File path to Clipboard
        StringSelection str = new StringSelection(Upload_ImagePath());
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(str, null);
     
         // press Contol+V for pasting
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
     
        // release Contol+V for pasting
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyRelease(KeyEvent.VK_V);
     
        // for pressing and releasing Enter
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        System.out.println("uploaded file");
			
	}
	
	 public String Upload_ImagePath() {
			Properties prop = new Properties();
			try {
				FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "\\src\\main\\resources\\TestData.Properties");
				try {
					prop.load(fis);
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
			} 
			catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			return prop.getProperty("upload_image_Path");
		}
	
	 public void verify_UploadedImage() {
			try {
				if (driver.findElement(Uploadedimage).isDisplayed()) {
					System.out.println("Successfully uploaded image");
				}
			} catch (Exception e) {
				System.out.println("Not uploaded files.");
				Assert.fail();
			}
		}

	 
	 public void DeleteImage() throws Exception {
			wdc.waitForExpactedElement_150(driver.findElement(DeleteImage));
			actions.click(driver.findElement(DeleteImage)).build().perform();
			System.out.println("System clicked On Delete button");
			wdc.waitForPageLoad();
				
		}
	 
	 
	 public void verify_DeletedImage() {
		 try {
				if (driver.findElement(Uploadedimage).isDisplayed()) {
					System.out.println("Image is not deleted");
					Assert.fail();
				}
			} catch (Exception e) {
				System.out.println("Image is deleted successfully.");
				
			}
	 }
}
