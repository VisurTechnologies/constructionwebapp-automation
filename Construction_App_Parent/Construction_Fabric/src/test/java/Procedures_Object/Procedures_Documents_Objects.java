package Procedures_Object;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Utils.BaseDriver;
import Utils.Constant;
import Utils.WebDriverCommonLib;

public class Procedures_Documents_Objects extends BaseDriver{

	public Procedures_Documents_Objects(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	Actions actions = new Actions(driver);
	WebDriverWait wait = new WebDriverWait(driver, 2);
	WebDriverCommonLib wdc = new WebDriverCommonLib(driver);
	JavascriptExecutor js = (JavascriptExecutor) driver; 

	public By ExapndHeader = By.xpath("//*[text()='HEADER']/ancestor::div[2]//*[@class='exp-icon toggle-icon']");
	public By procedureslist = By.xpath("//*[@id='dragtarget']//div[2]/datatable-body-cell[1]/div/span");
	public By ProcedureDocumentsExpandButton = By.xpath("//*[text()='PROCEDURE DOCUMENTS']/ancestor::div[2]//*[@class='exp-icon toggle-icon']");
	public By ProcedureDocumentsAddbutton = By.xpath("//*[text()='PROCEDURE DOCUMENTS']/ancestor::div[2]//*[contains(@name,'plus')]");
	public By foldericon = By.xpath("//*[@class='storage text']//*[contains(@class,'folder-icon')]");
	public By constructionappicon = By.xpath("//*[@id='ProcedureDocument']//*[contains(@class,'folder-icon')]");
	public By documentsicon = By.xpath("//*[text()='Documents']/ancestor::div[2]//*[contains(@class,'folder-icon')]");
	public By projectsicon = By.xpath("//*[text()='Projects']/ancestor::div[2]//*[contains(@class,'folder-icon')]");
	public By specification = By.xpath("//*[text()='Specifications']/ancestor::div[2]//*[contains(@class,'folder-icon')]");
	public By NewFolder = By.xpath("//*[text()='1Automation_Qa_Folder']/ancestor::div[2]//*[contains(@class,'folder-icon')]");
	public By Createfolder = By.xpath("//*[text()='Specifications']/ancestor::div/following-sibling::div[contains(@class,'document')]//*[@ng-reflect-message='create Folder']");
	public By FolderName = By.xpath("//*[@placeholder='Folder Name']");
	public By Save = By.xpath("//*[@class='form']//*[@ng-reflect-message='SAVE']");
	public By ClickonEdit = By.xpath("//*[text()='1Automation_Folder']/ancestor::div/following-sibling::div[contains(@class,'document')]//*[@ng-reflect-message='Edit']");
	public By Rename = By.xpath("//*[@placeholder='Rename']");
    public By FoldersList = By.xpath("//*[text()='Specifications']/ancestor::div/following-sibling::div//*[@class='nodeText title']");
    public By renamesaveForm = By.xpath("//*[@class='rename-form']//*[@ng-reflect-message='SAVE']");
    public By createfile = By.xpath("//*[text()='1Automation_Qa_Folder']/ancestor::div/following-sibling::div[contains(@class,'document')]//*[@ng-reflect-message='create File']");
	public By FileName = By.xpath("//*[@placeholder='File Name']");
	public By filelist = By.xpath("//*[text()='Specifications']/ancestor::div[contains(@class,'tree-node-expanded')]//*[@class='nodeText title']");
	public By fileedit = By.xpath("//*[text()='11Automation_File']/ancestor::div/following-sibling::div[contains(@class,'document')]//*[@ng-reflect-message='Edit']");
	public By closeAdddocumentform = By.xpath("//*[@class='form']//*[@ng-reflect-message='CLOSE']");
	public By delete =By.xpath("//*[text()='11Automation_Qa_File']/ancestor::div/following-sibling::div[contains(@class,'document')]//*[@ng-reflect-message='Delete']");
	public By Agree = By.xpath("//*[@class='header-popup']//*[@ng-reflect-message='AGREE']");
    public By DeletFolder = By.xpath("//*[text()='1Automation_Qa_Folder']/ancestor::div/following-sibling::div[contains(@class,'document')]//*[@ng-reflect-message='Delete']");
    public By uploadedfile = By.xpath("//*[@id='dragtarget']");
	public By ClickonView = By.xpath("//*[text()='remove_red_eye']");
    public By Browsefile = By.xpath("//*[@class='browseFile']");
    public By DeleteDocuments = By.xpath("//*[text()='DELETE DOCUMENTS']");
    public By headerform = By.xpath("//*[@id='construction-form-header']");
    public By verifyDocument = By.xpath("//*[@id='viewerContainer']");
    public By DeleteProcedure = By.xpath("//*[text()='DELETE PROCEDURE']");
    
    
    
    public void uploadfile() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(Browsefile));
		actions.click(driver.findElement(Browsefile)).build().perform();
		System.out.println("System clicked On Browsefile");
		  Robot robot = new Robot();
		     
	        // copying File path to Clipboard
	        StringSelection str = new StringSelection(Upload_FilePath());
	        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
	        clipboard.setContents(str, null);
	     
	         // press Contol+V for pasting
	        robot.keyPress(KeyEvent.VK_CONTROL);
	        robot.keyPress(KeyEvent.VK_V);
	     
	        // release Contol+V for pasting
	        robot.keyRelease(KeyEvent.VK_CONTROL);
	        robot.keyRelease(KeyEvent.VK_V);
	     
	        // for pressing and releasing Enter
	        robot.keyPress(KeyEvent.VK_ENTER);
	        robot.keyRelease(KeyEvent.VK_ENTER);
	        System.out.println("uploaded file");
			 
	}
   
    
    
    public void DeleteDocuments() {
    	wdc.waitForExpactedElement_150(driver.findElement(DeleteDocuments));
		actions.click(driver.findElement(DeleteDocuments)).build().perform();
		System.out.println("System clicked On Delete Documents");
		wdc.waitForPageLoad();
    } 
    
    public String Upload_FilePath() {
		Properties prop = new Properties();
		try {
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir") + "\\src\\main\\resources\\TestData.Properties");
			try {
				prop.load(fis);
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return prop.getProperty("upload_File_Path");
	}
  
    
    public void verify_UploadedFile(String s) throws InterruptedException {
    	JavascriptExecutor js = (JavascriptExecutor) driver;
		List<WebElement> UploadedFilesList = driver.findElements(uploadedfile);
		System.out.println("Producers list started" +UploadedFilesList.size());
		int i=0;
		for(WebElement e:UploadedFilesList) {
			System.out.println(e.getText());
			Thread.sleep(4000);
			if(e.getText().equals(s)) {
				System.out.println("uploaded file is displayed"+e.getText());
				
				wdc.waitForPageLoad();
				break;
			}
			js.executeScript("arguments[0].scrollIntoView();", UploadedFilesList.get(i));
			i++;
		}
	}
	
    public void verify_DeletedFile() {
    	try {
			if (driver.findElement(uploadedfile).isDisplayed()) {
				System.out.println("File is Not Deleted");
				Assert.fail();
			}
		} catch (Exception e) {
			System.out.println("File is deleted successfully");
			
		}
	}
    
    public void rightclickonfile() {
    	wdc.waitForExpactedElement_150(driver.findElement(uploadedfile));
		actions.contextClick(driver.findElement(uploadedfile)).build().perform();
		System.out.println("Right clicked on Uploaded file");
		wdc.waitForPageLoad();
    }
    
    public void clickonViewDocument() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(ClickonView));
		actions.click(driver.findElement(ClickonView)).build().perform();
		System.out.println("System clicked On View(eye) button ");
		wdc.waitForPageLoad();
			 
	}
    public void verifyDocument() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(verifyDocument));
		actions.click(driver.findElement(verifyDocument)).build().perform();
		System.out.println("Document is displaying ");
		wdc.waitForPageLoad();
			 
	}
    public void clickonFolderDelete() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(DeletFolder));
		actions.click(driver.findElement(DeletFolder)).build().perform();
		System.out.println("System clicked On Folder Delete button ");
			 
	}
    
	public void clickonDeleteinPopup() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(Agree));
		actions.click(driver.findElement(Agree)).build().perform();
		System.out.println("System clicked On Agree button ");
			 
	}
	
	public void clickonFileDeleteButton() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(delete));
		actions.click(driver.findElement(delete)).build().perform();
		System.out.println("System clicked On delete button ");
			 
	}
	
	public void closeAdddocumentform() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(closeAdddocumentform));
		actions.click(driver.findElement(closeAdddocumentform)).build().perform();
		System.out.println("System clicked On close button in Adddocumentform");
			 
	}
	public void ProceduresList(String s) throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		List<WebElement> procedureslistcheck = driver.findElements(procedureslist);
		System.out.println("Producers list started" +procedureslistcheck.size());
		int i=0;
		for(WebElement e:procedureslistcheck) {
			System.out.println(e.getText());
			Thread.sleep(4000);
			if(e.getText().equals(s)) {
				System.out.println(e.getText());
				actions.click(e).build().perform();
				System.out.println("clicked on a procedure");
				wdc.waitForPageLoad();
				 try {
						if (driver.findElement(headerform).isDisplayed()) {
							System.out.println("Header form is displayed");
							
						}
					} catch (Exception y) {
						System.out.println("Header form is not displayed");
						driver.navigate().refresh();
						
					}
			 }
				break;
			}
			js.executeScript("arguments[0].scrollIntoView();", procedureslistcheck.get(i));
			i++;
		
	}
	
	public void Procedureclick(String s) throws Exception {
		List<WebElement> procedureslistcheck = driver.findElements(procedureslist);
		int procedurescount = procedureslistcheck.size();
		System.out.println("Producers list started" +procedureslistcheck.size());
		wdc.waitForPageLoad();
		for(int i = 0; i < procedurescount; i++) {
			List<WebElement> procedureslist = driver.findElements(By.xpath("//*[@id='dragtarget']//div[2]/datatable-body-cell[1]/div/span"));
			WebElement procedure = procedureslist.get(i);
            Thread.sleep(3000);
			if(procedure.getText().equals(s)) {
				System.out.println(procedure.getText());
				actions.click(procedure).build().perform();
				wdc.waitForPageLoad();
				System.out.println("clicked on a procedure");
				
				break;
			}
			i++;
			js.executeScript("arguments[0].scrollIntoView();", procedureslistcheck.get(i));
			wdc.waitForPageLoad();
		}
		
	}
	
	public void RightClickOnProcedures(String s) throws Exception {
		List<WebElement> procedureslistcheck = driver.findElements(procedureslist);
		int procedurescount = procedureslistcheck.size();
		System.out.println("Producers list started" +procedureslistcheck.size());
		wdc.waitForPageLoad();
		for(int i = 0; i < procedurescount; i++) {
			List<WebElement> procedureslist = driver.findElements(By.xpath("//*[@id='dragtarget']//div[2]/datatable-body-cell[1]/div/span"));
			WebElement procedure = procedureslist.get(i);
            Thread.sleep(3000);
			if(procedure.getText().equals(s)) {
				System.out.println(procedure.getText());
				actions.contextClick(procedure).build().perform();
				wdc.waitForPageLoad();
				System.out.println("clicked on a procedure");
				
				break;
			}
			i++;
			js.executeScript("arguments[0].scrollIntoView();", procedureslistcheck.get(i));
			wdc.waitForPageLoad();
		}
		
	}
	
	public void ClickonDeleteProcedure() {
  		wdc.waitForExpactedElement_150(driver.findElement(DeleteProcedure));
  		if(driver.findElement(DeleteProcedure).isDisplayed()) {
  			System.out.println("Context menu is displayed with Delete Procedure option");
  			actions.click(driver.findElement(DeleteProcedure)).build().perform();
  			
  		}else{
  			System.out.println("Context menu is not displayed with Delete procedure option");
  		}
	}
	
	public void VerifyDeletedProcedure(String s) throws Exception {
		List<WebElement> procedureslistcheck = driver.findElements(procedureslist);
		int procedurescount = procedureslistcheck.size();
		System.out.println("Producers list started" +procedureslistcheck.size());
		wdc.waitForPageLoad();
		for(int i = 0; i < procedurescount; i++) {
			List<WebElement> procedureslist = driver.findElements(By.xpath("//*[@id='dragtarget']//div[2]/datatable-body-cell[1]/div/span"));
			WebElement procedure = procedureslist.get(i);
            Thread.sleep(3000);
			if(procedure.getText().equals(s)) {
				System.out.println("Procedure is not deleted");
				Assert.fail();
				
				break;
			}
			i++;
			js.executeScript("arguments[0].scrollIntoView();", procedureslistcheck.get(i));
			wdc.waitForPageLoad();
		}
		System.out.println("Procedure is deleted");
	}
	
	public void clickOnProcedureDocumentationExpand() throws Exception {
		wdc.imlicitlywaitfor_10();
		wdc.waitForExpactedElement_150(driver.findElement(ProcedureDocumentsExpandButton));
		actions.click(driver.findElement(ProcedureDocumentsExpandButton)).build().perform();
		System.out.println("System clicked On ProcedureDocuments ExpandButton");
			
	}
	public void clickOnProcedureDocumentationAddbutton() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(ProcedureDocumentsAddbutton));
		actions.click(driver.findElement(ProcedureDocumentsAddbutton)).build().perform();
		System.out.println("System clicked On ProcedureDocuments Addbutton");
		wdc.waitForPageLoad();
			
	}
	
	public void clickOnProcedurefoldericon() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(foldericon));
		actions.click(driver.findElement(foldericon)).build().perform();
		System.out.println("System clicked On folderi con");
			 
	}
	
	public void clickOnProcedureconstructionicon() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(constructionappicon));
		actions.click(driver.findElement(constructionappicon)).build().perform();
		System.out.println("System clicked On constructionapp icon");
			
	}
	
	public void clickOnDocumenticon() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(documentsicon));
		actions.click(driver.findElement(documentsicon)).build().perform();
		System.out.println("System clicked On documents icon");
			
	}
	
	public void clickOnprojects() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(projectsicon));
		actions.click(driver.findElement(projectsicon)).build().perform();
		System.out.println("System clicked On projects icon");
			
	}
	
	public void clickOnSpecifications() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(specification));
		actions.click(driver.findElement(specification)).build().perform();
		System.out.println("System clicked On specification icon");
			
	}
	public void clickOnCreatedFolder() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(NewFolder));
		actions.click(driver.findElement(NewFolder)).build().perform();
		System.out.println("System clicked On 11Automation_q_folder icon");
			
	}
	public void ClickonCreatefolder() {
		wdc.waitForExpactedElement_150(driver.findElement(Createfolder));
		actions.click(driver.findElement(Createfolder)).build().perform();
		System.out.println("System clicked On Createfolder icon");
			
	}
	
	public void EnterFolderName(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(FolderName));
		actions.click(driver.findElement(FolderName)).sendKeys(s).build().perform();
		System.out.println("Entered Folder Name");
			
	}
		
	public void ClickonSave() {
		wdc.waitForExpactedElement_150(driver.findElement(Save));
		actions.click(driver.findElement(Save)).build().perform();
		System.out.println("System clicked On Save button");
			
	}
	
	public void ClickOnFolderEdit() {
		wdc.waitForExpactedElement_150(driver.findElement(ClickonEdit));
		actions.click(driver.findElement(ClickonEdit)).build().perform();
		System.out.println("System clicked On Edit button");
		
	}
	
	public void ClickOnFileEdit() {
		wdc.waitForExpactedElement_150(driver.findElement(fileedit));
		actions.click(driver.findElement(fileedit)).build().perform();
		System.out.println("System clicked On Edit button");
		
	}
	public void EnterReName(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(Rename));
		actions.click(driver.findElement(Rename)).sendKeys(s).build().perform();
		System.out.println("Entered Rename");
			
	}
	
	public void Folderslist(String s) throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		wdc.waitForPageLoad(); 
		List<WebElement> Fileslist = driver.findElements(FoldersList);
		System.out.println(Fileslist.size());
		wdc.waitForPageLoad(); 
		int i=0;
		for(WebElement e:Fileslist) {
			System.out.println("Folder is = "+e.getText());
			Thread.sleep(2000);
			if (e.getText().equalsIgnoreCase(s)) {
				System.out.println("Successfully Created Folder.");
				break;
			}
			i++;
			js.executeScript("arguments[0].scrollIntoView();", Fileslist.get(i));
		}
	}
	
	public void verifyCreatedFolders(String s) throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		wdc.waitForPageLoad(); 
		List<WebElement> Fileslist = driver.findElements(filelist);
		System.out.println(Fileslist.size());
		wdc.waitForPageLoad(); 
		int i=0;
		for(WebElement e:Fileslist) {
			System.out.println("Folder is = "+e.getText());
			Thread.sleep(2000);
			if (e.getText().equalsIgnoreCase(s)) {
				System.out.println("Successfully Created Folder.");
				break;
			}
			i++;
			js.executeScript("arguments[0].scrollIntoView();", Fileslist.get(i));
		}
		
	}
	
	public void verifyupadtedfolder(String s) throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		wdc.waitForPageLoad(); 
		List<WebElement> Fileslist = driver.findElements(filelist);
		System.out.println(Fileslist.size());
		wdc.waitForPageLoad(); 
		int i=0;
		for(WebElement e:Fileslist) {
			System.out.println("Folder is = "+e.getText());
			Thread.sleep(2000);
			if (e.getText().equalsIgnoreCase(s)) {
				System.out.println("Successfully upadted Folder name.");
				break;
			}
			i++;
			js.executeScript("arguments[0].scrollIntoView();", Fileslist.get(i));
		}
	}
	
	
	public void verifyCreatedFile(String s) throws Exception {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		wdc.waitForPageLoad(); 
		List<WebElement> Fileslist = driver.findElements(FoldersList);
		System.out.println(Fileslist.size());
		wdc.waitForPageLoad(); 
		int i=0;
		for(WebElement e:Fileslist) {
			System.out.println("Folder is = "+e.getText());
			Thread.sleep(2000);
			if (e.getText().equalsIgnoreCase(s)) {
				System.out.println("Successfully upadted Folder name.");
				break;
			}
			i++;
			js.executeScript("arguments[0].scrollIntoView();", Fileslist.get(i));
		}
	}
	public void ClickonSaveRenameForm() {
		wdc.waitForExpactedElement_150(driver.findElement(renamesaveForm));
		actions.click(driver.findElement(renamesaveForm)).build().perform();
		System.out.println("System clicked On renamesaveForm button");
		wdc.waitForPageLoad();
		
			
	}
	public void ClickonCreatefile() {
		wdc.waitForExpactedElement_150(driver.findElement(createfile));
		actions.click(driver.findElement(createfile)).build().perform();
		System.out.println("System clicked On createfile icon");
		wdc.waitForPageLoad();
		
			
	}
	
	public void EnterFileName(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(FileName));
		actions.click(driver.findElement(FileName)).sendKeys(s).build().perform();
		System.out.println("Entered filename");
		wdc.waitForPageLoad();
		
		
	}
	
}
