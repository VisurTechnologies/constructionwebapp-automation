package Procedures_Object;

import static org.testng.Assert.fail;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Utils.BaseDriver;
import Utils.Constant;
import Utils.WebDriverCommonLib;

public class Procedures_Objects extends BaseDriver{
	public Procedures_Objects(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	LocalDate localDate = LocalDate.now();
	String s1 = DateTimeFormatter.ofPattern("yyy/MM/dd").format(localDate);

	Actions actions = new Actions(driver);
	WebDriverWait wait = new WebDriverWait(driver, 2);
	WebDriverCommonLib wdc = new WebDriverCommonLib(driver);
	JavascriptExecutor js = (JavascriptExecutor) driver; 
	
	public By Procedure = By.xpath("//*[@class='nav-bar']//*[8][@mattooltipposition='right']");
	public By AddButton = By.xpath("//*[contains(@name,'plus')]");
	public By Select_Category = By.xpath("//*[contains(text(),'Select Category')]");
	public By AddSpecificationform = By.xpath("//*[@id='addnew']");
	public By pipelineweldingProcedureDatasheet = By.xpath("//*[text()='AVAILABLE ITEMS']//ancestor::div[2]//*[text()=' Pipeline Welding Procedure Data Sheet ']");
	public By ExapndHeader = By.xpath("//*[text()='HEADER']/ancestor::div[2]//*[@class='exp-icon toggle-icon']");
	public By clickonpipelineweldingprocedure = By.xpath("//*[contains(@name,'specificationname')]");
	public By clickOnrevison = By.xpath("//*[contains(@name,'revision')]"); 
	public By servicetemparature = By.xpath("//*[contains(@name,'servicetemperature')]");
	public By weldingcode = By.xpath("//*[contains(@name,'weldingcode')]");
	public By ProjectApplication = By.xpath("//*[contains(@name,'projectapplication')]");
	public By Referencewelding = By.xpath("//*[contains(@name,'referenceweldingprocedurenumber')]"); 
	public By SupportingProcedure = By.xpath("//*[contains(@name,'supportingprocedurequalificationrecordlist')]");
	
	public By ExpandProcedureInformation = By.xpath("//*[text()='PROCEDURE INFORMATION']/ancestor::div[2]//*[contains(@class,'exp-icon toggle-icon')]");
	public By MaterialGuide = By.xpath("//*[contains(@name,'materialgrade')]");
	public By CarbonEquivalent = By.xpath("//*[contains(@name,'carbonequivalent')]");
	public By MaterialSizeDescription = By.xpath("//*[contains(@name,'materialsizedescription')]");
	public By PreheatTemparatureDescription = By.xpath("//*[contains(@name,'preheattemperaturedescription')]");
	public By interPassTemparatureDesc = By.xpath("//*[contains(@name,'interpasstemperaturedescription')]");
	public By Removaloflineupclamp = By.xpath("//*[contains(@name,'removaloflineupclamp')]");
	public By TimeintervalBetweenPasses = By.xpath("//*[contains(@name,'timeintervalbetweenpasses')]");
	public By AmountOfWelders = By.xpath("//*[contains(@name,'amountofwelders')]");
	public By weldingtechnique = By.xpath("//*[contains(@name,'weldingtechnique')]");
	public By weldingposition = By.xpath("//*[contains(@name,'weldingposition')]");
	public By weldingdirection = By.xpath("//*[contains(@name,'weldingdirection')]");
	public By jointgeometryandfitup = By.xpath("//*[contains(@name,'jointgeometryandfitup')]");
	public By jointcompletionandpasssequence = By.xpath("//*[contains(@name,'jointcompletionandpasssequence')]");
	
	public By ExpandCommonWeldingparameters = By.xpath("//*[text()='COMMON WELDING PARAMETERS']/ancestor::div[2]//*[contains(@class,'exp-icon toggle-icon')]");
	public By weldingprocesslist = By.xpath("//*[contains(@name,'weldingprocesslist')]");
	public By currentandpolarity = By.xpath("//*[contains(@name,'currentandpolarity')]");
	public By coatingtype = By.xpath("//*[contains(@name,'coatingtype')]");
	public By Alloytype = By.xpath("//*[contains(@name,'alloytype')]");
	
	public By ExpandWeldingparameters = By.xpath("//*[text()='WELDING PARAMETERS']/ancestor::div[2]//*[contains(@class,'exp-icon toggle-icon')]");
	public By Weldingparametersplusbutton = By.xpath("//*[text()='WELDING PARAMETERS']/ancestor::div[2]//*[contains(@name,'plus')]");
	public By Passorder = By.xpath("//*[contains(@name,'passorder')]");
	public By Weldpass = By.xpath("//*[contains(@name,'weldpass')]");
	public By Classificationgrade = By.xpath("//*[contains(@name,'classificationgrade')]");
	public By Classificationname = By.xpath("//*[contains(@name,'classificationname')]");
	public By Heatnumber = By.xpath("//*[contains(@name,'heatnumber')]");
	public By Diameter = By.xpath("//*[contains(@name,'diameter')]");
	public By Amperageminimum = By.xpath("//*[contains(@name,'amperageminimum')]");
	public By Amperagemaximum = By.xpath("//*[contains(@name,'amperagemaximum')]");
	public By Voltageminimum = By.xpath("//*[contains(@name,'voltageminimum')]");
	public By Voltagemaximum = By.xpath("//*[contains(@name,'voltagemaximum')]");
	public By Travelspeedminimum = By.xpath("//*[contains(@name,'travelspeedminimum')]");
	public By Travelspeedmaximum = By.xpath("//*[contains(@name,'travelspeedmaximum')]");
	public By Heatinputminimum = By.xpath("//*[contains(@name,'heatinputminimum')]");
	public By Heatinputmaximum = By.xpath("//*[contains(@name,'heatinputmaximum')]");
	public By feedspeedminimum = By.xpath("//*[contains(@name,'feedspeedminimum')]");
	public By feedspeedmaximum = By.xpath("//*[contains(@name,'feedspeedmaximum')]");
	public By gascomposition = By.xpath("//*[contains(@name,'gascomposition')]");
	public By gasflowrateminimum = By.xpath("//*[contains(@name,'gasflowrateminimum')]");
	public By gasflowratemaximum = By.xpath("//*[contains(@name,'gasflowratemaximum')]");
	public By ossilationrateminimum = By.xpath("//*[contains(@name,'ossilationrateminimum')]");
	public By ossilationratemaximum = By.xpath("//*[contains(@name,'ossilationratemaximum')]");
	public By contacttiptoworkdistanceminimum = By.xpath("//*[contains(@name,'contacttiptoworkdistanceminimum')]");
	public By contacttiptoworkdistancemaximum = By.xpath("//*[contains(@name,'contacttiptoworkdistancemaximum')]");
	public By timeintervalsincepreviouspassminiummum = By.xpath("//*[contains(@name,'timeintervalsincepreviouspassminimum')]");
	public By timeintervalsincepreviouspassmaximum = By.xpath("//*[contains(@name,'timeintervalsincepreviouspassmaximum')]");
	public By preheattemperatureminimum = By.xpath("//*[contains(@name,'preheattemperatureminimum')]");
	public By preheattemperaturemaximum = By.xpath("//*[contains(@name,'preheattemperaturemaximum')]");
	public By save = By.xpath("//*[@id='save']//*[contains(@class,'mat-icon')]");
	public By BackArrow = By.xpath("//*[@id='Backarrow']//*[contains(@class,'mat-icon')]");
	public By procedureslist = By.xpath("//*[@id='dragtarget']//div[2]/datatable-body-cell[1]/div/span");
	public By close = By.xpath("//*[@id='close']//*[contains(@class,'mat-icon')]");
	public By checkheader = By.xpath("//*[@id='save']//*[@ng-reflect-message='SAVE']");
    public By Availableitems = By.xpath("//*[text()='AVAILABLE ITEMS']");
    public By pipelineweldingProcedureDatasheet_nonfavouriteicon = By.xpath("//*[text()='AVAILABLE ITEMS']//ancestor::div[2]//*[text()=' Pipeline Welding Procedure Data Sheet ']//ancestor::div[2]//mat-icon");
    public By FavoritesAccordion= By.xpath("//*[text()='FAVORITES']//ancestor::div[2]//mat-expansion-panel-header");
    public By favorite_accordion_expand_button = By.xpath("//*[text()='FAVORITES']//ancestor::div[2]//div[contains(@class,'exp-icon')]");
    public By FavoritesAccordion_check_data = By.xpath("//*[text()='FAVORITES']//ancestor::div[2]//*[text()=' Pipeline Welding Procedure Data Sheet ']//ancestor::div[2]");
    public By SpecificationClose = By.xpath("//*[@class='form']//*[@ng-reflect-message='CLOSE']");
    public By ProcedureContextmenu = By.xpath("");
    
    
    public void SpecificationClose() throws Exception {
    	wdc.waitForExpactedElement_150(driver.findElement(SpecificationClose));
		actions.click(driver.findElement(SpecificationClose)).build().perform();
		System.out.println("System clicked On Close");
			
	}
    
    public void CheckDataInFavourites() throws Exception {
		if(driver.findElement(FavoritesAccordion_check_data).isDisplayed()) {
			System.out.println("Pipeline welding procedure data sheet is moved to  Fav");
			
		}else{
			System.out.println("pipeline welding procedure data sheet is in fav");
		}
			
	}
    public void PipelineWeldingProcedureDataSheet_favouritesChecking() throws Exception {
		if(driver.findElement(pipelineweldingProcedureDatasheet_nonfavouriteicon).getText().equals("star_border")) {
			System.out.println("Pipeline welding procedure data sheet is Non Fav");
			driver.findElement(pipelineweldingProcedureDatasheet_nonfavouriteicon).click();
			System.out.println("clicked on favorites icon");
			
		}else{
			System.out.println("pipeline welding procedure data sheet is in fav");
		}
			
	}
    
    public void CheckingFavouritesaccordion_expanded() throws Exception {
    	if(driver.findElement(FavoritesAccordion).getAttribute("aria-expanded").equals("false")) {
			System.out.println("fav accordion not expanded");
			driver.findElement(favorite_accordion_expand_button).click();
			System.out.println("Favorites accordion is expanded");
			Thread.sleep(2000);
		}

    }
    public void Availableitems() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(Availableitems));
		if(driver.findElement(Availableitems).isDisplayed()) {
			System.out.println("Available items accordions is displayed");
		}else{
			System.out.println("Available items accordions is not displayed");
		}
			
	}
    
    public void checkingPipelineweldingproceduredDatasheet() throws Exception {
  		wdc.waitForExpactedElement_150(driver.findElement(pipelineweldingProcedureDatasheet));
  		if(driver.findElement(pipelineweldingProcedureDatasheet).isDisplayed()) {
  			System.out.println("Available items accordions is displayed with pipelineweldingProcedureDatasheet");
  			
  		}else{
  			System.out.println("pipelineweldingProcedureDatasheet is not displaying");
  		}
  			
  	}
	public void ProceduresList() throws Exception {
		Thread.sleep(10000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		List<WebElement> procedureslistcheck = driver.findElements(procedureslist);
		int i=0;
		for(WebElement e :procedureslistcheck) {
			System.out.println(e.getText());
			try {
				if(e.getText().equalsIgnoreCase(Constant.Automation_procedure)) {
					System.out.println("Created procedure is displaying"+e.getText());
					break;
				}
				js.executeScript("arguments[0].scrollIntoView();", procedureslistcheck.get(i));
				i++;
			} catch (Exception e2) {
				Assert.fail();
							}
			
		}
		Thread.sleep(5000);
	
	}
	
	public void clickOnsave() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(save));
		actions.click(driver.findElement(save)).build().perform();
		System.out.println("System clicked On save");
		Thread.sleep(10000);	
	}
	public void clickOnclose() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(close));
		actions.click(driver.findElement(close)).build().perform();
		System.out.println("System clicked On close");
		wdc.waitForPageLoad();
	}
	
	public void VerifyHeaderform() throws Exception {
		wdc.imlicitlywaitfor_10();
		driver.navigate().refresh();
		System.out.println("Application refreshed ");
		 
	 
		
	}
	
	public void ClickBackArrow() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(BackArrow));
		actions.click(driver.findElement(BackArrow)).build().perform();
		System.out.println("System clicked On BackArrow");
		wdc.imlicitlywaitfor_10();
	}
	public void clickOnProcedure() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(Procedure));
		actions.click(driver.findElement(Procedure)).build().perform();
		System.out.println("System clicked On Procedure");
	}
	public void clickOnAddButton() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(AddButton));
		actions.click(driver.findElement(AddButton)).build().perform();
		System.out.println("System clicked On AddButton");
	}
	
	public void ClickonSelectCategory(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(Select_Category));
		actions.click(driver.findElement(Select_Category)).sendKeys(s).sendKeys(Keys.ENTER).build().perform();
        System.out.println("click on Select category");
        
	}
	
	public void ClickOnPipelineWeldingProcedureDataSheet() throws InterruptedException {
		wdc.waitForExpactedElement_150(driver.findElement(pipelineweldingProcedureDatasheet));
		actions.click(driver.findElement(pipelineweldingProcedureDatasheet)).build().perform();
        System.out.println("clicked on pipelineweldingProcedureDatasheet");
        Thread.sleep(5000);
	}
	
	public void ClickonheaderExapand() throws InterruptedException {
		wdc.waitForExpactedElement_150(driver.findElement(ExapndHeader));
		actions.click(driver.findElement(ExapndHeader)).build().perform();
        System.out.println("Expanded Header");
        Thread.sleep(6000);
        
	}
	
	public void clickonpipelineweldingprocedure(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(clickonpipelineweldingprocedure));
		actions.click(driver.findElement(clickonpipelineweldingprocedure)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("enterred value in onpipelineweldingprocedure field ");
        wdc.imlicitlywaitfor_10();
        
	}
	
	public void clickOnrevison(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(clickOnrevison));
		actions.click(driver.findElement(clickOnrevison)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println(" Entered value in revison field ");
        wdc.imlicitlywaitfor_10();
        
	}
	
	public void clickOnServiceTemparature(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(servicetemparature));
		actions.click(driver.findElement(servicetemparature)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value servicetemparature field");
        wdc.imlicitlywaitfor_10();
        
	}
	
	public void clickOnwelding(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(weldingcode));
		actions.click(driver.findElement(weldingcode)).sendKeys(s).sendKeys(Keys.ENTER).build().perform();
        System.out.println("Entered value weldingcode field");
        wdc.imlicitlywaitfor_10();
        
	}
	
	public void ClickonProjectapplication(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(ProjectApplication));
		actions.click(driver.findElement(ProjectApplication)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value ProjectApplication field");
        wdc.imlicitlywaitfor_10();
        
	}
	
	public void ClickonReferencewelsing(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(Referencewelding));
		actions.click(driver.findElement(Referencewelding)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value Referencewelding field");
        wdc.imlicitlywaitfor_10();
        
	}
	
	public void ClickonSupportingprocedure(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(SupportingProcedure));
		actions.click(driver.findElement(SupportingProcedure)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value SupportingProcedure field");
        wdc.imlicitlywaitfor_10();
        
	}
	
	public void ExpandProcedureInformation() throws InterruptedException {
		wdc.waitForExpactedElement_150(driver.findElement(ExpandProcedureInformation));
		actions.click(driver.findElement(ExpandProcedureInformation)).build().perform();
        System.out.println("Expanded Procedure Information Accordion");
        Thread.sleep(5000);
	}
	
	public void EnteringValueinMaterialGuide(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(MaterialGuide));
		actions.click(driver.findElement(MaterialGuide)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in Material Guide");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinCarbonEquivalent(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(CarbonEquivalent));
		actions.click(driver.findElement(CarbonEquivalent)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in CarbonEquivalent");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinMaterialSizeDescription(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(MaterialSizeDescription));
		actions.click(driver.findElement(MaterialSizeDescription)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in MaterialSizeDescription");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinPreheatTemparatureDescription(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(PreheatTemparatureDescription));
		actions.click(driver.findElement(PreheatTemparatureDescription)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in PreheatTemparatureDescription");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueininterPassTemparatureDesc(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(interPassTemparatureDesc));
		actions.click(driver.findElement(interPassTemparatureDesc)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in interPassTemparatureDesc");
        wdc.imlicitlywaitfor_10();
		
	}
	
	
	public void EnteringValueinRemovaloflineupclamp(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(Removaloflineupclamp));
		actions.click(driver.findElement(Removaloflineupclamp)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in Removaloflineupclamp");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinTimeintervalBetweenPasses(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(TimeintervalBetweenPasses));
		actions.click(driver.findElement(TimeintervalBetweenPasses)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in TimeintervalBetweenPasses");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinAmountOfWelders(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(AmountOfWelders));
		actions.click(driver.findElement(AmountOfWelders)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in AmountOfWelders");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinweldingtechnique(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(weldingtechnique));
		actions.click(driver.findElement(weldingtechnique)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in weldingtechnique");
        wdc.imlicitlywaitfor_10();
		
	}
	
	
	public void EnteringValueinweldingposition(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(weldingposition));
		actions.click(driver.findElement(weldingposition)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in weldingposition");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinweldingdirection(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(weldingdirection));
		actions.click(driver.findElement(weldingdirection)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in weldingdirection");
        wdc.imlicitlywaitfor_10();
		
	}

	public void EnteringValueinjointgeometryandfitup(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(jointgeometryandfitup));
		actions.click(driver.findElement(jointgeometryandfitup)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in jointgeometryandfitup");
        wdc.imlicitlywaitfor_10();
		
	}

	public void EnteringValueinjointcompletionandpasssequence(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(jointcompletionandpasssequence));
		actions.click(driver.findElement(jointcompletionandpasssequence)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in jointcompletionandpasssequence");
        wdc.imlicitlywaitfor_10();
        js.executeScript("arguments[0].scrollIntoView();", driver.findElement(ExpandProcedureInformation));
		
	}

	public void EnteringValueinweldingprocesslist(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(weldingprocesslist));
		actions.click(driver.findElement(weldingprocesslist)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in weldingprocesslist");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueincurrentandpolarity(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(currentandpolarity));
		actions.click(driver.findElement(currentandpolarity)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in currentandpolarity");
        wdc.imlicitlywaitfor_10();
		
	}
	public void EnteringValueincoatingtype(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(coatingtype));
		actions.click(driver.findElement(coatingtype)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in coatingtype");
        wdc.imlicitlywaitfor_10();
		
	}
	public void EnteringValueinAlloytype(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(Alloytype));
		actions.click(driver.findElement(Alloytype)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in Alloytype");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void ExpandCommonWeldingparameters() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(ExpandCommonWeldingparameters));
		actions.click(driver.findElement(ExpandCommonWeldingparameters)).build().perform();
        System.out.println("Expanded CommonWeldingparameters");
        Thread.sleep(5000);
	}
	
	public void ExpandExpandWeldingparameters() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(ExpandWeldingparameters));
		actions.click(driver.findElement(ExpandWeldingparameters)).build().perform();
        System.out.println("Expanded ExpandWeldingparameters");
        Thread.sleep(7000);
	}
	
	public void Weldingparametersplusbutton() throws Exception {
		wdc.waitForExpactedElement_150(driver.findElement(Weldingparametersplusbutton));
		actions.click(driver.findElement(Weldingparametersplusbutton)).build().perform();
        System.out.println("Click on  Welding parameters plusbutton");
        wdc.imlicitlywaitfor_10();
	}
	
	public void EnteringValueinPassorder(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(Passorder));
		actions.click(driver.findElement(Passorder)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in Passorder");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinWeldpass(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(Weldpass));
		actions.click(driver.findElement(Weldpass)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in Weldpass");
        wdc.imlicitlywaitfor_10();
		
	}
	
	
	public void EnteringValueinClassificationgrade(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(Classificationgrade));
		actions.click(driver.findElement(Classificationgrade)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in Classificationgrade");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinClassificationname(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(Classificationname));
		actions.click(driver.findElement(Classificationname)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in Classificationname");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinHeatnumber(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(Heatnumber));
		actions.click(driver.findElement(Heatnumber)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in Heatnumber");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinDiameter(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(Diameter));
		actions.click(driver.findElement(Diameter)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in Diameter");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinAmperageminimum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(Amperageminimum));
		actions.click(driver.findElement(Amperageminimum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in Amperageminimum");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinAmperagemaximum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(Amperagemaximum));
		actions.click(driver.findElement(Amperagemaximum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in Amperagemaximum");
        wdc.imlicitlywaitfor_10();
		
	}
	public void EnteringValueinTravelspeedminimum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(Travelspeedminimum));
		actions.click(driver.findElement(Travelspeedminimum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in Voltagemaximum");
        wdc.imlicitlywaitfor_10();
		
	}
	public void EnteringValueinTravelspeedmaximum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(Travelspeedmaximum));
		actions.click(driver.findElement(Travelspeedmaximum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in Travelspeedmaximum");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinVoltageminimum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(Voltageminimum));
		actions.click(driver.findElement(Voltageminimum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in Voltageminimum");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinVoltagemaximum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(Voltagemaximum));
		actions.click(driver.findElement(Voltagemaximum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in Voltagemaximum");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinHeatinputminimum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(Heatinputminimum));
		actions.click(driver.findElement(Heatinputminimum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in Heatinputminimum");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinHeatinputmaximum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(Heatinputmaximum));
		actions.click(driver.findElement(Heatinputmaximum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in Heatinputmaximum");
        wdc.imlicitlywaitfor_10();
		
	}
	public void EnteringValueinfeedspeedminimum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(feedspeedminimum));
		actions.click(driver.findElement(feedspeedminimum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in feedspeedminimum");
        wdc.imlicitlywaitfor_10();
		
	}
	public void EnteringValueinfeedspeedmaximum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(feedspeedmaximum));
		actions.click(driver.findElement(feedspeedmaximum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in feedspeedmaximum");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueingascomposition(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(gascomposition));
		actions.click(driver.findElement(gascomposition)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in gascomposition");
        wdc.imlicitlywaitfor_10();
		
	}
	public void EnteringValueingasflowrateminimum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(gasflowrateminimum));
		actions.click(driver.findElement(gasflowrateminimum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in gasflowrateminimum");
        wdc.imlicitlywaitfor_10();
		
	}
	public void EnteringValueingasflowratemaximum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(gasflowratemaximum));
		actions.click(driver.findElement(gasflowratemaximum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in gasflowratemaximum");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinossilationrateminimum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(gasflowratemaximum));
		actions.click(driver.findElement(gasflowratemaximum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in gasflowratemaximum");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinossilationratemaximum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(ossilationratemaximum));
		actions.click(driver.findElement(ossilationratemaximum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in ossilationratemaximum");
        wdc.imlicitlywaitfor_10();
		
	}
	public void EnteringValueincontacttiptoworkdistanceminimum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(contacttiptoworkdistanceminimum));
		actions.click(driver.findElement(contacttiptoworkdistanceminimum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in contacttiptoworkdistanceminimum");
        wdc.imlicitlywaitfor_10();
		
	}
	public void EnteringValueincontacttiptoworkdistancemaximum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(contacttiptoworkdistancemaximum));
		actions.click(driver.findElement(contacttiptoworkdistancemaximum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in contacttiptoworkdistancemaximum");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueintimeintervalsincepreviouspassminiummum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(timeintervalsincepreviouspassminiummum));
		actions.click(driver.findElement(timeintervalsincepreviouspassminiummum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in timeintervalsincepreviouspassminiummum");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueintimeintervalsincepreviouspassmaximum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(timeintervalsincepreviouspassmaximum));
		actions.click(driver.findElement(timeintervalsincepreviouspassmaximum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in timeintervalsincepreviouspassmaximum");
        wdc.imlicitlywaitfor_10();
		
	}
	

	public void EnteringValueininpreheattemperatureminimum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(preheattemperatureminimum));
		actions.click(driver.findElement(preheattemperatureminimum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in preheattemperatureminimum");
        wdc.imlicitlywaitfor_10();
		
	}
	
	public void EnteringValueinpreheattemperaturemaximum(String s) {
		wdc.waitForExpactedElement_150(driver.findElement(preheattemperaturemaximum));
		actions.click(driver.findElement(preheattemperaturemaximum)).sendKeys(s).sendKeys(Keys.TAB).build().perform();
        System.out.println("Entered value in preheattemperaturemaximum");
        wdc.imlicitlywaitfor_10();
		
	}
	
	
}
