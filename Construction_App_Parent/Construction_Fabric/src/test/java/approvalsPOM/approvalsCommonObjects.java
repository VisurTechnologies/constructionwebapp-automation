package approvalsPOM;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.handler.WebDriverHandler;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utils.BaseDriver;
import Utils.WebDriverCommonLib;

public class approvalsCommonObjects extends BaseDriver{

	public approvalsCommonObjects(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	Actions actions = new Actions(driver);
	WebDriverCommonLib wdc = new WebDriverCommonLib(driver);
	

	By clickapprovalstypeoption = By.xpath("//*[@class='ng-select-container ng-has-value']");
	By clickexhibit = By.xpath("//*[@id='drop-block']//*[@id='dragtarget']");
	By approvals = By.xpath("//*[@class='nav-bar']//*[2][@mattooltipposition='right']");
	By Reviewcommentbox = By.xpath("//*[text()='Review Comments']/..//*[@data-placeholder='Insert text here ...']");
	By Approvalcommentbox=By.xpath("//*[text()='Comments']/..//*[@data-placeholder='Insert text here ...']");
	By approvalsname = By.xpath("//*[@data-placeholder='Approval Name']");
	By reviewbyname = By.xpath("//*[@placeholder='Review By']");
	By savebutton = By.xpath("//*[@class='head-route inactive-icon-opacity']");
	By closeform = By.xpath("//*[@id='construction-form-header']//*[@ng-reflect-message='CLOSE']");
	By SubmittalStatus =By.xpath("//*[text()='Submittal Status']/..//*[@role='combobox']");
	By ApprovalStatus =By.xpath("//*[text()='Approval Status']/..//*[@role='combobox']");
	By ReviewDelete =By.xpath("//*[@name='delete']");
	By VerifyDeleteReview=By.xpath("//*[contains(text(),'No data to show')]");
	By ReviewApprovalexpandCollapseAll =By.xpath("//*[contains(@id,'Review')]//*[contains(@id,'expandCollapseAll')]//*[contains(@class,'notranslate')]");
	By ReviewAddButton = By.xpath("//*[text()='REVIEWS']/ancestor::mat-expansion-panel//*[@name='plus']");
	By VerifyCreateReview = By.xpath("//*[text()='REVIEWS']/ancestor::mat-expansion-panel");
	
	
	
	
	public void clickonapprovalstab() throws Exception{
		try {
			do {
				driver.findElement(approvals).click();
				System.out.println("Approvals Tab Clicked.");
				try {
					if (driver.findElement(By.xpath("//*[text()=' Approvals ']")).isDisplayed()) {
						System.out.println("Successfully Logged In... ... ...");
						break;
					}
				} catch (Exception e) {
					driver.navigate().refresh();
					Thread.sleep(6000);
				}
			} while (!driver.findElement(approvals).getAttribute("class").contains("active"));
		} catch (Exception e) {
			System.out.println("Approvals Error!!! !!! !!! !!!");
		}
	}


	public void clickapprovaltypefield() throws Exception{
		WebElement clickapprolvals = driver.findElement(clickapprovalstypeoption);
		wdc.waitForExpactedElement_150(clickapprolvals);
		actions.click(clickapprolvals).build().perform();
	}
	
	public void selectapprovalstype(String s) throws Exception{
		List<WebElement> selectapprovals = driver.findElements(By.xpath("//*[@role='option']"));
		wdc.waitForExpectedAllElements(selectapprovals);
				
		int j = 0;
		for (WebElement e : selectapprovals) {
			if (e.getText().equals(s)) {
				actions.click(e).build().perform();
				break;
			}
			j++;
		}		
	}
	
	public void clickexhibit() throws Exception{
		List<WebElement> clickjounrnal = driver.findElements(clickexhibit);
		wdc.waitForExpectedAllElements(clickjounrnal);
		clickjounrnal.get(0).click();
		System.out.println("clicked on journal");
	}
	public void selectSubmittalStatus() throws Exception{
		wdc.waitForExpactedElement_150(driver.findElement(SubmittalStatus));
		actions.click(driver.findElement(SubmittalStatus)).build().perform();
		wdc.waitForPageLoad();
		List<WebElement> selectSubmittalStatus = driver.findElements(By.xpath("//*[@role='option']"));
		WebElement e = selectSubmittalStatus.get(4);
		wdc.waitForPageLoad();
		actions.moveToElement(e).click().build().perform();
		wdc.waitForPageLoad();
		System.out.println("Submitted Status selected");
	}
	public void selectApprovalStatus() {
		wdc.waitForExpactedElement_150(driver.findElement(ApprovalStatus));
		actions.click(driver.findElement(ApprovalStatus)).build().perform();
		wdc.waitForPageLoad();
		List<WebElement> selectApprovalStatus = driver.findElements(By.xpath("//*[@role='option']"));
		WebElement e = selectApprovalStatus.get(1);
		wdc.waitForPageLoad();
		actions.moveToElement(e).click().build().perform();
		wdc.waitForPageLoad();
		System.out.println("Approval Status selected");
	}
	
	public void reviews_comments(String s) throws Exception{
		List<WebElement> reviewcomment = driver.findElements(Reviewcommentbox);
		wdc.waitForExpectedAllElements(reviewcomment);
		WebElement e = reviewcomment.get(0);
		actions.doubleClick(e).sendKeys(s).sendKeys(Keys.TAB).build().perform();
		System.out.println("entered the comments in review comment box");
	}
	
	public void approval_comments(String s) throws Exception{
		List<WebElement> approvalcomment = driver.findElements(Approvalcommentbox);
		wdc.waitForExpectedAllElements(approvalcomment);		
		WebElement e = approvalcomment.get(1);
		actions.doubleClick(e).sendKeys(s).sendKeys(Keys.TAB).build().perform();
		System.out.println("entered the comments in approvals comment box");
	}
	
	public void approvalsname(String s) throws Exception{
		WebElement approvalname = driver.findElement(approvalsname);
		wdc.waitForExpactedElement_150(approvalname);
		actions.doubleClick(approvalname).sendKeys(s).sendKeys(Keys.TAB).build().perform();
	}
	
	public void reviewby(String s) throws Exception{
		WebElement reviewby = driver.findElement(reviewbyname);
		wdc.waitForExpactedElement_150(reviewby);
		actions.doubleClick(reviewby).sendKeys(s).sendKeys(Keys.TAB).build().perform();
	}
	
	public void savebutton() throws Exception{
		WebElement savebtn = driver.findElement(savebutton);
		wdc.waitForExpactedElement_150(savebtn);
		actions.click(savebtn).build().perform();
		System.out.println("System saved the Review and Approval Form");
	}
	
	public void formclosebutton() throws Exception{
		WebElement closebtn = driver.findElement(closeform);
		wdc.waitForExpactedElement_150(closebtn);
		actions.click(closebtn).build().perform();
		System.out.println("System Closed the Review and Approval Form");
	}
	public void reviewDelete() {
		WebElement ReviewDeletebutton=driver.findElement(ReviewDelete);
		wdc.waitForExpactedElement_150(ReviewDeletebutton);
		actions.click(ReviewDeletebutton).build().perform();
		System.out.println("Succesfully Deleted the Review Accordion");
	}
	public void verifyDeletedReview() {
		try {
			if(driver.findElement(VerifyDeleteReview).isDisplayed()){
				System.out.println("Succesfully Deleted the Review Accordion");
			}
		} catch (Exception e) {
			System.out.println("Review Accordion is not Deleted");
		}

	}
	public void ReviewApprovalexpandCollapseAll() {
		WebElement reviewApprovalexpandCollapseAll=driver.findElement(ReviewApprovalexpandCollapseAll);
		wdc.waitForExpactedElement_150(reviewApprovalexpandCollapseAll);
		actions.click(reviewApprovalexpandCollapseAll).build().perform();
	}
	
	public void switchToNextTab() throws Exception {
		ArrayList<String> listOfTab=new ArrayList<String>(driver.getWindowHandles());
		System.out.println(listOfTab.size()+" Total Size");
//		System.out.println(listOfTab.get(0));
//		System.out.println(listOfTab.get(1));
		listOfTab.remove(0);
		driver.switchTo().window(listOfTab.get(0));
		wdc.waitForPageLoad();
		System.out.println(driver.getCurrentUrl());	}
	
	public void createReviewAccordion() {
		WebElement createReview=driver.findElement(ReviewAddButton);
		wdc.waitForExpactedElement_150(createReview);
		actions.click(createReview).build().perform();
	}
	public void verifyCreatedReview() {
		try {
			if(driver.findElement(VerifyCreateReview).isDisplayed()){
				System.out.println("Created review accordion is  Displaying");
			}
		} catch (Exception e) {
			System.out.println("Review accordion not Created");
		}
	}
}