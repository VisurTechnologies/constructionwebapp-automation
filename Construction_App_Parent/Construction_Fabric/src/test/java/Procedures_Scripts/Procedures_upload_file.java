package Procedures_Scripts;

import org.testng.annotations.Test;

import Procedures_Object.Procedures_Documents_Objects;
import Procedures_Object.Procedures_Objects;
import SearchEntities.Searchentitiesobjects;
import Utils.Constant;
import Utils.LoginSetup;

public class Procedures_upload_file extends LoginSetup{

@Test(dependsOnMethods = {"Procedure_Attached_images_crud"})
public void Procedures_uploading_file() throws Exception {
	Procedures_Documents_Objects Procedures_Documents_Object = new Procedures_Documents_Objects(driver);
	Searchentitiesobjects searchentitiesobjects = new Searchentitiesobjects(driver);
	Procedures_Objects procedure_objects = new Procedures_Objects(driver);
	
	procedure_objects.clickOnProcedure();
	searchentitiesobjects.SearchAndClick_ProceduresProject(Constant.projecName);
	Procedures_Documents_Object.Procedureclick(Constant.Automation_procedure);
	procedure_objects.VerifyHeaderform();
	//Procedures_Documents_Object.clickOnProcedureDocumentationExpand();
	Procedures_Documents_Object.clickOnProcedureDocumentationAddbutton();
	Procedures_Documents_Object.uploadfile();
	procedure_objects.clickOnsave();
	Procedures_Documents_Object.verify_UploadedFile(Constant.Uploadedfilename);
	Procedures_Documents_Object.clickonViewDocument();
	Procedures_Documents_Object.verifyDocument();
	procedure_objects.clickOnclose();
	Procedures_Documents_Object.Procedureclick(Constant.Automation_procedure);
	procedure_objects.VerifyHeaderform();
	Procedures_Documents_Object.clickOnProcedureDocumentationExpand();
	Procedures_Documents_Object.verify_UploadedFile(Constant.Uploadedfilename);
//	Procedures_Documents_Object.rightclickonfile();
//	Procedures_Documents_Object.DeleteDocuments();
//	Procedures_Documents_Object.verify_DeletedFile();
	
	
	
	
	
	
	
	
  }
}
