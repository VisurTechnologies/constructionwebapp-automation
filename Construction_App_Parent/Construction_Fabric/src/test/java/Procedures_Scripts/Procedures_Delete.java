package Procedures_Scripts;

import org.testng.annotations.Test;

import Procedures_Object.Procedures_Documents_Objects;
import Procedures_Object.Procedures_Objects;
import SearchEntities.Searchentitiesobjects;
import Utils.Constant;
import Utils.LoginSetup;

public class Procedures_Delete extends LoginSetup {
	
	@Test(dependsOnMethods= { "Procedures_uploading_file" })
	public void Deleting_procedures() throws Exception { 
		Procedures_Objects procedure_objects = new Procedures_Objects(driver);
		Procedures_Documents_Objects Procedures_Documents_Object = new Procedures_Documents_Objects(driver);
		Searchentitiesobjects searchentitiesobjects = new Searchentitiesobjects(driver);
		
		procedure_objects.clickOnProcedure();
		searchentitiesobjects.SearchAndClick_ProceduresProject(Constant.projecName);
		Procedures_Documents_Object.RightClickOnProcedures(Constant.Automation_procedure);
		Procedures_Documents_Object.ClickonDeleteProcedure();
		Procedures_Documents_Object.clickonDeleteinPopup();
		procedure_objects.VerifyHeaderform();
		Procedures_Documents_Object.VerifyDeletedProcedure(Constant.Automation_procedure);
		
		//verifing the deleted procedure after close and open the form
		procedure_objects.clickOnclose();
		searchentitiesobjects.SearchAndClick_ProceduresProject(Constant.projecName);
		Procedures_Documents_Object.VerifyDeletedProcedure(Constant.Automation_procedure);
		
		
	}

}
