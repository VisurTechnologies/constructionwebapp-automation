package Procedures_Scripts;

import org.testng.annotations.Test;

import Procedures_Object.Procedures_Objects;
import SearchEntities.Searchentitiesobjects;
import Utils.Constant;
import Utils.LoginSetup;

public class Procedures_Specification_Favourites extends LoginSetup{
	@Test(dependsOnMethods = {"Procedures_uploading_file"})
	public void Specifications_favourites() throws Exception {
		Procedures_Objects procedure_objects = new Procedures_Objects(driver);
		Searchentitiesobjects searchentitiesobjects = new Searchentitiesobjects(driver);
		
		procedure_objects.clickOnProcedure();
		searchentitiesobjects.SearchAndClick_ProceduresProject(Constant.projecName);
		procedure_objects.clickOnAddButton();
		procedure_objects.ClickonSelectCategory(Constant.CategoryName);
		procedure_objects.Availableitems();
		procedure_objects.checkingPipelineweldingproceduredDatasheet();
		procedure_objects.PipelineWeldingProcedureDataSheet_favouritesChecking();
		procedure_objects.CheckingFavouritesaccordion_expanded();
		procedure_objects.CheckDataInFavourites();
		procedure_objects.SpecificationClose();
		
		//verifying the pipeline datasheet after close and open Add specification form
		procedure_objects.clickOnAddButton();
		procedure_objects.ClickonSelectCategory(Constant.CategoryName);
		procedure_objects.CheckingFavouritesaccordion_expanded();
		procedure_objects.CheckDataInFavourites();
		
		
	}

}
