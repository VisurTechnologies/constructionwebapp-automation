package Procedures_Scripts;

import org.testng.annotations.Test;

import Procedures_Object.Procedures_Documents_Objects;
import Procedures_Object.Procedures_Objects;
import SearchEntities.Searchentitiesobjects;
import Utils.Constant;
import Utils.LoginSetup;

public class Procedures_Documents_crud extends LoginSetup{
@Test(dependsOnMethods = {"Procedures"})
public void Procedures_Documents_crud_operations() throws Exception {
	Procedures_Objects procedure_objects = new Procedures_Objects(driver);
	Procedures_Documents_Objects Procedures_Documents_Object = new Procedures_Documents_Objects(driver);
	Searchentitiesobjects searchentitiesobjects = new Searchentitiesobjects(driver);
	Procedures_Creation_Scripts procedurecreation = new Procedures_Creation_Scripts();
	
	procedure_objects.clickOnProcedure();
	searchentitiesobjects.SearchAndClick_ProceduresProject(Constant.projecName);
	Procedures_Documents_Object.Procedureclick(Constant.Automation_procedure);
	Procedures_Documents_Object.clickOnProcedureDocumentationExpand();
	Procedures_Documents_Object.clickOnProcedureDocumentationAddbutton();
	Procedures_Documents_Object.clickOnProcedurefoldericon();
	Procedures_Documents_Object.clickOnProcedureconstructionicon();
	Procedures_Documents_Object.clickOnDocumenticon();
	Procedures_Documents_Object.clickOnprojects();
	Procedures_Documents_Object.clickOnSpecifications();
	
	//creating folder
	Procedures_Documents_Object.ClickonCreatefolder();
	Procedures_Documents_Object.EnterFolderName(Constant.FolderName);
	Procedures_Documents_Object.ClickonSave();
	Procedures_Documents_Object.Folderslist(Constant.FolderName);
	
	//update folder name
	Procedures_Documents_Object.ClickOnFolderEdit();
	Procedures_Documents_Object.EnterReName(Constant.Rename);
	Procedures_Documents_Object.ClickonSaveRenameForm();
	Procedures_Documents_Object.verifyupadtedfolder(Constant.Rename);
	
	//create file
	Procedures_Documents_Object.ClickonCreatefile();
	Procedures_Documents_Object.EnterFileName(Constant.FileName);
	Procedures_Documents_Object.ClickonSave();
	Procedures_Documents_Object.clickOnCreatedFolder();
	Procedures_Documents_Object.verifyCreatedFile(Constant.FileName);
	
	//update filename
	Procedures_Documents_Object.ClickOnFileEdit();
	Procedures_Documents_Object.EnterReName(Constant.RenameFile);
	Procedures_Documents_Object.ClickonSaveRenameForm();
	
	Procedures_Documents_Object.verifyCreatedFile(Constant.RenameFile);
	Procedures_Documents_Object.closeAdddocumentform();
	Procedures_Documents_Object.clickOnProcedureDocumentationAddbutton();
	Procedures_Documents_Object.clickOnProcedurefoldericon();
	Procedures_Documents_Object.clickOnProcedureconstructionicon();
	Procedures_Documents_Object.clickOnDocumenticon();
	Procedures_Documents_Object.clickOnprojects();
	Procedures_Documents_Object.clickOnSpecifications();
	Procedures_Documents_Object.clickOnCreatedFolder();
	Procedures_Documents_Object.verifyCreatedFolders(Constant.Rename);
	Procedures_Documents_Object.verifyCreatedFile(Constant.FileName);
	
	//delete file
	Procedures_Documents_Object.clickonFileDeleteButton();
	Procedures_Documents_Object.clickonDeleteinPopup();
	
	//delete folder
	Procedures_Documents_Object.clickonFolderDelete();
	Procedures_Documents_Object.clickonDeleteinPopup();
	Procedures_Documents_Object.closeAdddocumentform();
}
}
