package Procedures_Scripts;


import org.testng.annotations.Test;

import Procedures_Object.Procedures_Objects;
import SearchEntities.Searchentitiesobjects;
import Utils.Constant;
import Utils.LoginSetup;

public class Procedures_Creation_Scripts extends LoginSetup {
	
	@Test(groups = {"Regression Test"})
	public void Procedures() throws Exception {
		Procedures_Objects procedure_objects = new Procedures_Objects(driver);
		Searchentitiesobjects searchentitiesobjects = new Searchentitiesobjects(driver);
		
		System.out.println("Started creating procedure");
		procedure_objects.clickOnProcedure();
		searchentitiesobjects.SearchAndClick_ProceduresProject(Constant.projecName);
		procedure_objects.clickOnAddButton();
		procedure_objects.ClickonSelectCategory(Constant.CategoryName);
		procedure_objects.ClickOnPipelineWeldingProcedureDataSheet();
		
		
		//HEADER FORM
		
		procedure_objects.ClickonheaderExapand();
		procedure_objects.clickonpipelineweldingprocedure(Constant.specificationname);
		procedure_objects.clickOnrevison(Constant.revision);
		procedure_objects.clickOnServiceTemparature(Constant.Servicetemparature);
		procedure_objects.clickOnwelding(Constant.welder);
		procedure_objects.ClickonProjectapplication(Constant.ProjectDesc);
		procedure_objects.ClickonReferencewelsing(Constant.Referencewelding);
		procedure_objects.ClickonSupportingprocedure(Constant.supportingprocedure);
		procedure_objects.ClickonheaderExapand(); 
		
		//Procedure Information
		
		procedure_objects.ExpandProcedureInformation();
		procedure_objects.EnteringValueinMaterialGuide(Constant.MaterialGuide);
		procedure_objects.EnteringValueinCarbonEquivalent(Constant.Carbonequivalent);
		procedure_objects.EnteringValueinMaterialSizeDescription(Constant.MaterialSizeDescription);
		procedure_objects.EnteringValueinPreheatTemparatureDescription(Constant.PreheatTemparatureDescription);
		procedure_objects.EnteringValueininterPassTemparatureDesc(Constant.interPassTemparatureDesc);
		procedure_objects.EnteringValueinRemovaloflineupclamp(Constant.Removaloflineupclamp);
		procedure_objects.EnteringValueinTimeintervalBetweenPasses(Constant.TimeintervalBetweenPasses);
		procedure_objects.EnteringValueinAmountOfWelders(Constant.Amountofwelders);
		procedure_objects.EnteringValueinweldingtechnique(Constant.weldingtechnique);
		procedure_objects.EnteringValueinweldingposition(Constant.weldingposition);
		procedure_objects.EnteringValueinweldingdirection(Constant.weldingdirection);
		procedure_objects.EnteringValueinjointgeometryandfitup(Constant.jointgeometryandfitup);
		procedure_objects.EnteringValueinjointcompletionandpasssequence(Constant.jointcompletionandpasssequence);
		
		procedure_objects.ExpandProcedureInformation(); 
		
		//Common welding Parameters
		
		procedure_objects.ExpandCommonWeldingparameters();
		procedure_objects.EnteringValueinweldingprocesslist(Constant.weldingprocesslist);
		procedure_objects.EnteringValueincurrentandpolarity(Constant.currentandpolarity);
		procedure_objects.EnteringValueincoatingtype(Constant.coatingtype);
		procedure_objects.EnteringValueinAlloytype(Constant.Alloytype);
		procedure_objects.ExpandCommonWeldingparameters();
		
		//Welding parameters
		
		procedure_objects.ExpandExpandWeldingparameters();
		procedure_objects.Weldingparametersplusbutton();
		procedure_objects.EnteringValueinPassorder(Constant.Passorder);
		procedure_objects.EnteringValueinWeldpass(Constant.Weldpass);
		procedure_objects.EnteringValueinClassificationgrade(Constant.Classificationgrade);
		procedure_objects.EnteringValueinClassificationname(Constant.Classificationname);
		procedure_objects.EnteringValueinHeatnumber(Constant.Heatnumber);
		procedure_objects.EnteringValueinDiameter(Constant.Diameter);
		procedure_objects.EnteringValueinAmperageminimum(Constant.Amperageminimum);
		procedure_objects.EnteringValueinAmperagemaximum(Constant.Amperagemaximum);
		procedure_objects.EnteringValueinTravelspeedminimum(Constant.Travelspeedminimum);
		procedure_objects.EnteringValueinTravelspeedmaximum(Constant.Travelspeedmaximum);
		procedure_objects.EnteringValueinVoltageminimum(Constant.Voltageminimum);
		procedure_objects.EnteringValueinVoltagemaximum(Constant.Voltagemaximum);
		procedure_objects.EnteringValueinHeatinputminimum(Constant.Heatinputminimum);
		procedure_objects.EnteringValueinHeatinputmaximum(Constant.Heatinputmaximum);
		procedure_objects.EnteringValueinfeedspeedminimum(Constant.feedspeedminimum);
		procedure_objects.EnteringValueinfeedspeedmaximum(Constant.feedspeedmaximum);
		procedure_objects.EnteringValueingascomposition(Constant.gascomposition);
		procedure_objects.EnteringValueingasflowrateminimum(Constant.gasflowrateminimum);
		procedure_objects.EnteringValueingasflowratemaximum(Constant.gasflowratemaximum);
		procedure_objects.EnteringValueinossilationrateminimum(Constant.ossilationrateminimum);
		procedure_objects.EnteringValueinossilationratemaximum(Constant.ossilationratemaximum);
		procedure_objects.EnteringValueincontacttiptoworkdistanceminimum(Constant.contacttiptoworkdistanceminimum);
		procedure_objects.EnteringValueincontacttiptoworkdistancemaximum(Constant.contacttiptoworkdistancemaximum);
		procedure_objects.EnteringValueintimeintervalsincepreviouspassmaximum(Constant.timeintervalsincepreviouspassmaximum);
		procedure_objects.EnteringValueintimeintervalsincepreviouspassminiummum(Constant.timeintervalsincepreviouspassminiummum);
		procedure_objects.EnteringValueininpreheattemperatureminimum(Constant.preheattemperatureminimum);
		procedure_objects.EnteringValueinpreheattemperaturemaximum(Constant.preheattemperaturemaximum);
		procedure_objects.ExpandExpandWeldingparameters();
		procedure_objects.clickOnsave();
		procedure_objects.ClickBackArrow();
		
		//Verifying whether the procedure is created or not
		
		procedure_objects.ProceduresList();
	}
}
