package Procedures_Scripts;

import org.testng.annotations.Test;

import Procedures_Object.Procedure_attached_images_objects;
import Procedures_Object.Procedures_Documents_Objects;
import Procedures_Object.Procedures_Objects;
import SearchEntities.Searchentitiesobjects;
import Utils.Constant;
import Utils.LoginSetup;

public class Procedure_Attached_images extends LoginSetup{
	
	
	@Test(dependsOnMethods = {"Procedures_Documents_crud_operations"})
	public void Procedure_Attached_images_crud() throws Exception {
		Procedures_Documents_Objects Procedures_Documents_Object = new Procedures_Documents_Objects(driver);
		Searchentitiesobjects searchentitiesobjects = new Searchentitiesobjects(driver);
		Procedure_attached_images_objects procedure_attached_images_objects = new Procedure_attached_images_objects(driver);
		Procedures_Objects procedure_objects = new Procedures_Objects(driver);
		
		//upload image
		procedure_objects.clickOnProcedure();
		searchentitiesobjects.SearchAndClick_ProceduresProject(Constant.projecName);
		Procedures_Documents_Object.Procedureclick(Constant.Automation_procedure);
		procedure_objects.VerifyHeaderform();
		procedure_attached_images_objects.clickOnProcedure_Attached_image_Addbutton();
		procedure_attached_images_objects.UploadImage();
		procedure_objects.clickOnsave();
		procedure_attached_images_objects.verify_UploadedImage();
		procedure_attached_images_objects.CheckImageDate();
		procedure_attached_images_objects.CheckImageName();
		procedure_objects.clickOnclose();
		Procedures_Documents_Object.Procedureclick(Constant.Automation_procedure);
		procedure_objects.VerifyHeaderform();
		procedure_attached_images_objects.clickOnProcedure_Attached_image_EXPAND();
		procedure_attached_images_objects.verify_UploadedImage();
		//Deleting image
		procedure_attached_images_objects.DeleteImage();
		procedure_attached_images_objects.verify_DeletedImage();
		
		
		
	}
	
	
	
	

	

}
