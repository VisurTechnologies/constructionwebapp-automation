package approvalsTypeScripts;

import org.testng.annotations.Test;

import SearchEntities.Searchentitiesobjects;
import Utils.Constant;
import Utils.LoginSetup;
import approvalsPOM.approvalsCommonObjects;

public class ExhibitsTypeApprovals extends LoginSetup {
	
	@Test(groups = {"Sanity"})
	public void exhibitsTypeApprovals() throws Exception {
		
		System.out.println("ExhibitsTypeApprovals---Execution Started");
		approvalsCommonObjects approvalscommon = new approvalsCommonObjects(driver);
		Searchentitiesobjects searchentitiesobjects =new Searchentitiesobjects(driver);
		
		approvalscommon.clickonapprovalstab();
		searchentitiesobjects.searchAndclick_Approvals_project(Constant.Projectname);
		approvalscommon.clickapprovaltypefield();
		approvalscommon.selectapprovalstype(Constant.Environment);
		approvalscommon.clickexhibit();
		approvalscommon.switchToNextTab();
		approvalscommon.selectSubmittalStatus();
		approvalscommon.selectApprovalStatus();
		approvalscommon.reviews_comments(Constant.reviewcommant);
		approvalscommon.approval_comments(Constant.approvalscomment);
		approvalscommon.approvalsname(Constant.approvalsname);
		approvalscommon.reviewby(Constant.reviewby);
		approvalscommon.savebutton();
		approvalscommon.reviewDelete();
		approvalscommon.verifyDeletedReview();
		approvalscommon.savebutton();
		approvalscommon.createReviewAccordion();
		approvalscommon.savebutton();
		approvalscommon.verifyCreatedReview();
		approvalscommon.formclosebutton();
		
		System.out.println("ExhibitsTypeApprovals---Execution Successfull");
		
	}

}
