package directory_Object;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Utils.BaseDriver;
import Utils.WebDriverCommonLib;

public class Directory_Objects extends BaseDriver{

	public Directory_Objects(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	WebDriverCommonLib wdc =new WebDriverCommonLib(driver);
	Actions actions=new Actions(driver);
	WebDriverCommonLib wait=new WebDriverCommonLib(driver);
	JavascriptExecutor js = (JavascriptExecutor) driver;


	public By DirectoryTab = By.xpath("//*[@class='nav-bar']//*[7][@mattooltipposition='right']");
	public By PeopleCompanies=By.xpath("//*[@class='nav-bar']//*[contains(@ng-reflect-message, 'PEOPLE & COMPANIES')]");
	public By PMAdminExpandButton=By.xpath("//*[text()='PROJECT MANAGER ADMIN']/..//*[@class='exp-icon toggle-icon']//i-icons");
	By SearchIcon = By.xpath("//*[@id='filter-search']//mat-icon[@svgicon='MainSearch']");
	public By ScheduleDataIsBeingLoaded = By.xpath("//div[@id='Schedule']/../*[normalize-space(text())='DATA IS BEING LOADED']");
	public By rightTree_SearchIcon=By.xpath("//*[@id='right-filter-search']//mat-icon[@svgicon='MainSearch']");
	public By rightTree_FilterBox=By.xpath("//*[@id='right-filter-search']//*[@placeholder='  Filter']");
	public By FilterTextBox = By.xpath("//*[@placeholder='   Filter']");
	public By destinationLOCperson=By.xpath("//*[@class='mat-table']//*[contains(@class,'projectmanageradmin')]");
	public By ClickOnSaveButton=By.xpath("//*[@class='head-route full-opacity']//*[contains(@data-mat-icon-name,'FDC_Checkmark')]");
	public By PersonName=By.xpath("");
	public By VerifyPerson=By.xpath("//*[contains(@class,'datatable-row-center')]//datatable-body-cell[1]//*[contains(@class,'mat-tooltip-trigger datatable')]");
	public By projectAccessStatus=By.xpath("//*[contains(@class,'datatable-row-center')]//datatable-body-cell[3]//*[contains(@class,'mat-tooltip-trigger datatable')]");
	public By EntityManagementbutton=By.xpath("//*[@id='Entity Management']");
	public By ClickOnMenuButton=By.xpath("//*[@mattooltip='MENU']");
	public By ClickOnPeopleandCompanyTab=By.xpath("//*[@ng-reflect-message='PEOPLE & COMPANIES']");
	public By ClickOnConstructionTab=By.xpath("//*[@class='mat-tab-list']//*[@ng-reflect-message='Construction']");
	public By VerifyProjectRole=By.xpath("//*[@id='drop-block']//*[text()=' TestProject ']");
	public By Constructionbutton=By.xpath("//*[@id='header']//*[@id='Construction']");
	public By clickonDeactivate=By.xpath("//*[text()='PROJECT ACCESS DEACTIVATE']");
	public By clickonActivate=By.xpath("//*[text()='PROJECT ACCESS ACTIVATE']");
	public By EMprojectAccessStatus=By.xpath("//*[text()='PROJECTS ROLE']/ancestor::div//*[contains(@class,'datatable-row-center')]//datatable-body-cell[4]");

	public void clickOnDirectoryTab() {
		try {
			do {
				driver.findElement(DirectoryTab).click();
				System.out.println("DirectoryTab  Clicked.");
				try {
					if (driver.findElement(By.xpath("//*[text()=' Directory ']")).isDisplayed()) {
						System.out.println("Successfully Logged In... ... ...");
						break;
					}
				} catch (Exception e) {
					driver.navigate().refresh();
					Thread.sleep(6000);
				}
			} while (!driver.findElement(DirectoryTab).getAttribute("class").contains("active"));
		} catch (Exception e) {
			System.out.println("DirectoryTab Error!!! !!! !!! !!!");
		}
	}
	public void waitForDataLoading() {
		try {
			wdc.waitForPageLoad();
			if(driver.findElement(ScheduleDataIsBeingLoaded).isDisplayed()) {			
				System.out.println("DataIsBeingLoaded is displaying");
				WebDriverWait wait = new WebDriverWait(driver, 60);
				wait.until(ExpectedConditions.invisibilityOf(driver.findElement(ScheduleDataIsBeingLoaded)));
			}
		}
		catch(Exception e) {
			System.out.println("DATA IS BEING LOADED is not displaying");
		}
	}

	public void searchAndclick_Directory_project(String s) throws Exception {
		boolean obj=true;
		int i=0;
		while (obj) {
			if (i<3) {
				try {
					System.out.println("search");
					waitForDataLoading();
					wdc.waitForExpactedElement(driver.findElement(SearchIcon));
					if (driver.findElement(SearchIcon).isDisplayed()) {
						System.out.println(" Search icon displayed.");
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						actions.click(driver.findElement(SearchIcon)).build().perform();
						System.out.println("searchicon");
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					}


					wdc.waitForExpactedElement(driver.findElement(FilterTextBox));
					if (driver.findElement(FilterTextBox).isDisplayed()) {
						System.out.println("Construction Filter Box Displayed.");
						wdc.waitForExpactedElement(driver.findElement(FilterTextBox));
						actions.click(driver.findElement(FilterTextBox)).sendKeys(s).sendKeys(Keys.ENTER).build().perform();
						wdc.waitForPageLoad();
						Thread.sleep(2000);

					}

					wdc.waitForPageLoad();	

					System.out.println("Trying to  click on a project... ...");
					wdc.waitForPageLoad();
					List<WebElement> nodetext=driver.findElements(By.xpath("//div[@id='Directory']//*[@class='node-wrapper']//*[contains(@class,'nodeText title')]"));	
					//List<WebElement> nodeAllTree=driver.findElements(By.xpath("//div[@id='People & Companies']//*[@class='angular-tree-component']//tree-node"));
					String hightOfTree=driver.findElement(By.xpath("//div[@id='Directory']//*[contains(@style, 'height')]")).getCssValue("height")
							.replace(" ", "").replaceAll("([a-z])", "");
					System.out.println("Tree hight is after search : "+ hightOfTree);
					int hight=Integer.parseInt(hightOfTree);
					wdc.imlicitlywaitfor_80();
					while (nodetext.size()!=0 && hight<250) {
						for(WebElement e:nodetext) {
							System.out.println("project==== "+e.getText());
							Thread.sleep(2000);
							if(e.getText().equalsIgnoreCase(s)) {			
								actions.click(e).build().perform();
								System.out.println("project clicked.");
								wdc.imlicitlywaitfor_80();
								Thread.sleep(3000);
								driver.navigate().refresh();
								Thread.sleep(3000);
								wdc.waitForPageLoad();
								break;
							}
						}
						break;
					}


					wdc.imlicitlywaitfor_80();
					if (driver.findElement(By.xpath("//*[contains(@id, 'form-header')] ")).isDisplayed() || driver.findElement(By.xpath("//*[@id='construction-form-body']")).isDisplayed()) {
						System.out.println("form displayed.");
						obj=false;
					}
				}
				catch (Exception e) {
					Thread.sleep(3000);	
				}
			}
			else {
				obj=false;
				System.out.println("Searching Failed Due Tree Not Loading !!! !!! !!! !!!");
			}
		}
	}

	public void clickOnPeopleCompanies() {
		wdc.waitForExpactedElement_150(driver.findElement(PeopleCompanies));
		actions.click(driver.findElement(PeopleCompanies)).build().perform();
		System.out.println("Click On People & Companies");
		wdc.imlicitlywaitfor_20();
	}

	public void pmAdminExpandButton() throws Exception {
		waitForDataLoading();
		wdc.waitForExpactedElement_150(driver.findElement(PMAdminExpandButton));
		actions.click(driver.findElement(PMAdminExpandButton)).build().perform();
		System.out.println("Expand the PM admin accordion");
		wdc.imlicitlywaitfor_80();

	}


	public void searchPeopleAndCompanies(String s) throws Exception {
		boolean obj=true;
		int i=0;
		while (obj) {
			if (i<3) {
				try {
					System.out.println("search");
					waitForDataLoading();
					wdc.waitForExpactedElement(driver.findElement(rightTree_SearchIcon));
					if (driver.findElement(rightTree_SearchIcon).isDisplayed()) {
						System.out.println(" Search icon displayed.");
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						actions.click(driver.findElement(rightTree_SearchIcon)).build().perform();
						System.out.println("searchicon");
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					}


					wdc.waitForExpactedElement(driver.findElement(rightTree_FilterBox));
					if (driver.findElement(rightTree_FilterBox).isDisplayed()) {
						System.out.println("Construction Filter Box Displayed.");
						wdc.waitForExpactedElement(driver.findElement(rightTree_FilterBox));
						actions.click(driver.findElement(rightTree_FilterBox)).sendKeys(s).sendKeys(Keys.ENTER).build().perform();
						wdc.waitForPageLoad();
						Thread.sleep(3000);
					}

					wdc.waitForPageLoad();	

					wdc.waitForPageLoad();
					List<WebElement> nodetext=driver.findElements(By.xpath("//*[@class='right-tree-body scrollbar']//*[contains(@class,'nodeText title')]"));	
					//List<WebElement> nodeAllTree=driver.findElements(By.xpath("//div[@id='People & Companies']//*[@class='angular-tree-component']//tree-node"));
					String hightOfTree=driver.findElement(By.xpath("//*[@class='right-tree-body scrollbar']//*[contains(@style, 'height')]")).getCssValue("height")
							.replace(" ", "").replaceAll("([a-z])", "");
					System.out.println("Tree hight is after search : "+ hightOfTree);
					int hight=Integer.parseInt(hightOfTree);
					wdc.imlicitlywaitfor_80();
					while (nodetext.size()!=0 && hight<250) {
						for(WebElement e:nodetext) {
							System.out.println("project==== "+e.getText());
							Thread.sleep(2000);
							if(e.getText().equals(s)) {			
								System.out.println("System Displaying Person Name");
								drag_and_drop(e,driver.findElement(destinationLOCperson));
								wdc.imlicitlywaitfor_150();
								break;
							}
						}
						break;
					}


					wdc.imlicitlywaitfor_80();
					if (driver.findElement(By.xpath("//*[contains(@id, 'form-header')] ")).isDisplayed() || driver.findElement(By.xpath("//*[@id='construction-form-body']")).isDisplayed()) {
						System.out.println("form displayed.");
						obj=false;
					}
				}
				catch (Exception e) {
					Thread.sleep(3000);
				}
			}
			else {
				obj=false;
				System.out.println("Searching Failed Due Tree Not Loading !!! !!! !!! !!!");
			}

		}
	}

	public void drag_and_drop(WebElement source,WebElement Destination) {

		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("function createEvent(typeOfEvent) {\n" + "var event =document.createEvent(\"CustomEvent\");\n"
				+ "event.initCustomEvent(typeOfEvent,true, true, null);\n" + "event.dataTransfer = {\n" + "data: {},\n"
				+ "setData: function (key, value) {\n" + "this.data[key] = value;\n" + "},\n"
				+ "getData: function (key) {\n" + "return this.data[key];\n" + "}\n" + "};\n" + "return event;\n"
				+ "}\n" + "\n" + "function dispatchEvent(element, event,transferData) {\n"
				+ "if (transferData !== undefined) {\n" + "event.dataTransfer = transferData;\n" + "}\n"
				+ "if (element.dispatchEvent) {\n" + "element.dispatchEvent(event);\n"
				+ "} else if (element.fireEvent) {\n" + "element.fireEvent(\"on\" + event.type, event);\n" + "}\n"
				+ "}\n" + "\n" + "function simulateHTML5DragAndDrop(element, destination) {\n"
				+ "var dragStartEvent =createEvent('dragstart');\n" + "dispatchEvent(element, dragStartEvent);\n"
				+ "var dropEvent = createEvent('drop');\n"
				+ "dispatchEvent(destination, dropEvent,dragStartEvent.dataTransfer);\n"
				+ "var dragEndEvent = createEvent('dragend');\n"
				+ "dispatchEvent(element, dragEndEvent,dropEvent.dataTransfer);\n" + "}\n" + "\n"
				+ "var source = arguments[0];\n" + "var destination = arguments[1];\n"
				+ "simulateHTML5DragAndDrop(source,destination);", source, Destination);

	}





	public void dragPerson() throws Exception {
		for(int i=0; i<1;i++) {
			Thread.sleep(2000);
			List<WebElement> list=driver.findElements(PersonName);
			drag_and_drop(list.get(i),driver.findElement(destinationLOCperson));
			wdc.imlicitlywaitfor_80();
		}


	}
	public void clickOnSaveButton () {
		wdc.waitForPageLoad();
		wdc.waitForExpactedElement_150(driver.findElement(ClickOnSaveButton));
		actions.click(driver.findElement(ClickOnSaveButton)).build().perform();
		System.out.println("Click On SaveButton");
		wdc.imlicitlywaitfor_80();
	}




	public void verifyPersonIsDisplay(String s) {
		try {
			wdc.waitForPageLoad();
			List<WebElement> list=driver.findElements(VerifyPerson);
			int i=0;
			for(WebElement e:list) {
				wdc.waitForPageLoad();
				if(e.getText().equals(s)) {
					System.out.println("Person is Displaying");
					System.out.println("Person Name = "+ e.getText());
					actions.click(list.get(i)).build().perform();
					wdc.waitForPageLoad();
					break;
				}
				i++;
				js.executeScript("arguments[0].scrollIntoView();", list.get(i));	
			}
		}
		catch (Exception e) {
			System.out.println("Person is not Displaying");
		}
	}

	public void clickOnMenuButton(){
		wdc.waitForPageLoad();
		driver.findElement(ClickOnMenuButton).click();
		wdc.waitForPageLoad();
		System.out.println("Menu button clicked.");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	public void switchToEM() {
		waitForDataLoading();
		driver.findElement(EntityManagementbutton).click();
		System.out.println("Entity Management Button clicked.");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		wdc.imlicitlywaitfor_80();
	}

	public void clickOnPeopleandCompanyTab() {
		try {
			do {
				waitForDataLoading();
				driver.findElement(ClickOnPeopleandCompanyTab).click();
				waitForDataLoading();
				System.out.println("People and companies tab clicked");
				try {
					if (driver.findElement(By.xpath("//*[text()=' People & Companies ']")).isDisplayed()) {
						System.out.println("People and companies displayed");
						break;
					}
				} catch (Exception e) {
					driver.navigate().refresh();
					Thread.sleep(6000);
				}
			} while (driver.findElement(ClickOnPeopleandCompanyTab).getAttribute("class").equalsIgnoreCase("mat-tooltip-trigger headerIcon ng-star-inserted"));
		} catch (Exception e) {
			System.out.println("People and companies tab not clicked");
		}
	}

	public void searchAndclick_PeopleAndComapniesTab(String s) throws Exception {
		boolean obj=true;
		int i=0;
		while (obj) {
			if (i<3) {
				try {
					System.out.println("search");
					waitForDataLoading();
					wdc.waitForExpactedElement(driver.findElement(SearchIcon));
					if (driver.findElement(SearchIcon).isDisplayed()) {
						System.out.println(" Search icon displayed.");
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						actions.click(driver.findElement(SearchIcon)).build().perform();
						System.out.println("searchicon");
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					}


					wdc.waitForExpactedElement(driver.findElement(FilterTextBox));
					if (driver.findElement(FilterTextBox).isDisplayed()) {
						System.out.println("Construction Filter Box Displayed.");
						wdc.waitForExpactedElement(driver.findElement(FilterTextBox));
						actions.click(driver.findElement(FilterTextBox)).sendKeys(s).sendKeys(Keys.ENTER).build().perform();
						wdc.waitForPageLoad();
						Thread.sleep(2000);

					}

					wdc.waitForPageLoad();	

					System.out.println("Trying to  click on a person name... ...");
					wdc.waitForPageLoad();
					List<WebElement> nodetext=driver.findElements(By.xpath("//div[@id='People & Companies']//*[@class='node-wrapper']//*[contains(@class,'nodeText title')]"));	
					//List<WebElement> nodeAllTree=driver.findElements(By.xpath("//div[@id='People & Companies']//*[@class='angular-tree-component']//tree-node"));
					String hightOfTree=driver.findElement(By.xpath("//div[@id='People & Companies']//*[contains(@style, 'height')]")).getCssValue("height")
							.replace(" ", "").replaceAll("([a-z])", "");
					System.out.println("Tree hight is after search : "+ hightOfTree);
					int hight=Integer.parseInt(hightOfTree);
					wdc.imlicitlywaitfor_80();
					while (nodetext.size()!=0 && hight<250) {
						for(WebElement e:nodetext) {
							System.out.println("project==== "+e.getText());
							Thread.sleep(2000);
							if(e.getText().equalsIgnoreCase(s)) {			
								actions.click(e).build().perform();
								System.out.println("Person clicked.");
								wdc.imlicitlywaitfor_80();
								Thread.sleep(15000);
								break;
							}
						}
						break;
					}


					wdc.imlicitlywaitfor_80();
					if (driver.findElement(By.xpath("//*[contains(@id, 'form-header')] ")).isDisplayed() || driver.findElement(By.xpath("//*[@id='EM-Form-Gen']")).isDisplayed()) {
						System.out.println("form displayed.");
						obj=false;
					}
				}
				catch (Exception e) {
					Thread.sleep(3000);	
				}
			}
			else {
				obj=false;
				System.out.println("Searching Failed Due Tree Not Loading !!! !!! !!! !!!");
			}
		}
	}
	public void clickOnConstructionTab() throws Exception {
		Thread.sleep(10000);
		wdc.waitForPageLoad();
		driver.findElement(ClickOnConstructionTab).click();
		System.out.println("Construction tab clicked");
		driver.manage().timeouts().implicitlyWait(25,TimeUnit.SECONDS);   	
	}

	public void verifyProjectRoleIsDisplay(String s) {
		try {
			wdc.waitForPageLoad();
			List<WebElement> list=driver.findElements(VerifyProjectRole);
			int i=0;
			for(WebElement e:list) {
				wdc.waitForPageLoad();
				if(e.getText().equals(s)) {
					System.out.println("Project Role is Displaying");
					System.out.println("Role Name = "+e.getText());
					actions.click(list.get(i)).build().perform();
					Thread.sleep(5000);
					wdc.waitForPageLoad();
					break;
				}
				i++;
				js.executeScript("arguments[0].scrollIntoView();", list.get(i));	
			}
		}
		catch (Exception e) {
			System.out.println("Project Role is not Displaying");
		}
	}
	public void switchToConstruction() {
		driver.findElement(Constructionbutton).click();
		System.out.println("Construction Button clicked.");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		wdc.imlicitlywaitfor_80();
	}
	public void rightclick(String s) {
		wdc.waitForPageLoad();
		try {
			wdc.waitForPageLoad();
			List<WebElement> list=driver.findElements(VerifyPerson);
			int i=0;
			for(WebElement e:list) {
				wdc.waitForPageLoad();
				if(e.getText().equals(s)) {
					System.out.println("Person is Displaying");
					System.out.println("Person Name = "+e.getText());
					actions.contextClick(list.get(i)).build().perform();
					Thread.sleep(2000);
					wdc.waitForPageLoad();
					break;
				}
				i++;
				js.executeScript("arguments[0].scrollIntoView();", list.get(i));	
			}
		}
		catch (Exception e) {
			System.out.println("Person is not Displaying");
		}  

	}

	public void clickonActivate(String s) throws Exception {
		wdc.waitForPageLoad();
		driver.findElement(clickonActivate).click();
		wdc.waitForPageLoad();
		Thread.sleep(2000);
		try {
			wdc.waitForPageLoad();
			List<WebElement> list=driver.findElements(VerifyPerson);
			List<WebElement> listStatus=driver.findElements(projectAccessStatus);
			int i=0;
			for(WebElement e:list) {
				wdc.waitForPageLoad();
				if(e.getText().equals(s)) {
					System.out.println("Person Name = "+e.getText());
					if (listStatus.get(i).getText().equalsIgnoreCase("true")) {
						System.out.println("Successfully ACTIVATED.");
						wdc.waitForPageLoad();
						break;
					}
				}
				i++;
				js.executeScript("arguments[0].scrollIntoView();", list.get(i));	
			}
		}
		catch (Exception e) {
			System.out.println("Person is not Displaying");
		}  
	}

	public void EMActivateCheck() {
		String value=driver.findElement(EMprojectAccessStatus).getText();
		if (value.equalsIgnoreCase("true")) {
			System.out.println("Project Access Status field is displaying True");

		}
		else {
			Assert.fail();
			System.out.println("Project Access Status fields True is not displaying.");
		}
	}
	  
	
	public void clickonDeactivate(String s) throws Exception {
		wdc.waitForPageLoad();
		driver.findElement(clickonDeactivate).click();
		wdc.waitForPageLoad();
		Thread.sleep(2000);
		try {
			wdc.waitForPageLoad();
			List<WebElement> list=driver.findElements(VerifyPerson);
			List<WebElement> listStatus=driver.findElements(projectAccessStatus);
			int i=0;
			for(WebElement e:list) {
				wdc.waitForPageLoad();
				if(e.getText().equals(s)) {
					System.out.println("Person Name = "+e.getText());
					if (listStatus.get(i).getText().equalsIgnoreCase("false")) {
						System.out.println("Successfully DEACTIVATED.");
						wdc.waitForPageLoad();
						break;
					}
				}
				i++;
				js.executeScript("arguments[0].scrollIntoView();", list.get(i));	
			}
		}
		catch (Exception e) {
			System.out.println("Person is not Displaying");
		}
     }
	public void EMDEActivateCheck() {
		String value=driver.findElement(EMprojectAccessStatus).getText();
		if (value.equalsIgnoreCase("false")) {
			System.out.println("Project Access Status field is displaying False");

		}
		else {
			Assert.fail();
			System.out.println("Project Access Status fields False is not displaying.");
		}
	}
}


