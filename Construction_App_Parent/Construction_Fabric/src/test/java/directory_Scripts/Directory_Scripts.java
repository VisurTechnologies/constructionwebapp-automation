package directory_Scripts;
import org.testng.annotations.Test;
import Utils.Constant;
import Utils.LoginSetup;
import directory_Object.Directory_Objects;

public class Directory_Scripts extends LoginSetup {

	
	@Test
	public void directory_Scripts() throws Exception {
		System.out.println("Directory_Scripts---Execution Started");
		Directory_Objects directory_Objects =new Directory_Objects(driver);
		
		directory_Objects.clickOnDirectoryTab();
		directory_Objects.searchAndclick_Directory_project(Constant.Projectname);
		directory_Objects.clickOnPeopleCompanies();
		directory_Objects.pmAdminExpandButton();
		/*
		 * directory_Objects.searchPeopleAndCompanies(Constant.Peoplename);
		 
		 */
		directory_Objects.verifyPersonIsDisplay(Constant.Peoplename);
		/* directory_Objects.clickOnSaveButton(); */
		directory_Objects.clickOnMenuButton();
		directory_Objects.switchToEM();
		directory_Objects.clickOnPeopleandCompanyTab();
		directory_Objects.searchAndclick_PeopleAndComapniesTab(Constant.Peoplename);
		directory_Objects.clickOnConstructionTab();
		directory_Objects.verifyProjectRoleIsDisplay(Constant.ProjectRole);
		directory_Objects.clickOnMenuButton();
		directory_Objects.switchToConstruction();
		directory_Objects.clickOnDirectoryTab();
		directory_Objects.searchAndclick_Directory_project(Constant.Projectname);
		directory_Objects.pmAdminExpandButton();
		directory_Objects.rightclick(Constant.Peoplename);
		directory_Objects.clickonActivate(Constant.Peoplename);
		directory_Objects.clickOnSaveButton();
		directory_Objects.clickOnMenuButton();
		directory_Objects.switchToEM();
		directory_Objects.clickOnPeopleandCompanyTab();
		directory_Objects.searchAndclick_PeopleAndComapniesTab(Constant.Peoplename);
		directory_Objects.clickOnConstructionTab();
		directory_Objects.EMActivateCheck();
		directory_Objects.clickOnMenuButton();
		directory_Objects.switchToConstruction();
		directory_Objects.clickOnDirectoryTab();
		directory_Objects.searchAndclick_Directory_project(Constant.Projectname);
		directory_Objects.pmAdminExpandButton();
		directory_Objects.rightclick(Constant.Peoplename);
		directory_Objects.clickonDeactivate(Constant.Peoplename);
		directory_Objects.clickOnSaveButton();
		directory_Objects.clickOnMenuButton();
		directory_Objects.switchToEM();
		directory_Objects.clickOnPeopleandCompanyTab();
		directory_Objects.searchAndclick_PeopleAndComapniesTab(Constant.Peoplename);
		directory_Objects.clickOnConstructionTab();
		directory_Objects.EMDEActivateCheck();
	}
}
