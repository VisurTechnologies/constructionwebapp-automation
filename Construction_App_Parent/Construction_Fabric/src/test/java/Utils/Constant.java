
package Utils;

public class Constant {


	
	public static final String OperationLocation = "INSPECTIONFORMS_CREATION";

	public static final String project = "TestProject";
	public static final String folderName = "Automation_Folder";
	public static final String parentfolderName = "Automation_ParentFolder";
	public static final String Type = "Exhibits";
	public static final String Projectname = "TestProject";
	public static final String Peoplename = "MASS MANAS";
	public static final String ProjectRole = "TestProject";  
	
	
	public static final String Journals = "Journals";
	public static final String Safety = "Safety";
	public static final String Environment = "Environment";
	public static final String Exhibits = "Exhibits";
	
	public static final String reviewcommant = "Testing comment in review accordion";
	public static final String approvalscomment = "Testing comment in approval accordion";
	public static final String approvalsname = "Automation Testing";
	public static final String reviewby = "Automation Tester";
	
	public static final String submittalStatus = "yes";
	
	public static final String CategoryName = "welding";
	public static final String specificationname = "Automation_pipeline";

	public static final String revision = "12345";
	public static final String Servicetemparature = "12";
	public static final String welder = "Automan";
	public static final String ProjectDesc = "MR GAT The application is Good";
	public static final String Referencewelding = "Reference welding";
	public static final String supportingprocedure = "Reference welding";
	public static final String MaterialGuide ="Automation_Procedures";
	public static final String Carbonequivalent ="Carbon-W739%$#";
	public static final String MaterialSizeDescription = "MaterialSizeDescription is 8.8";
	public static final String PreheatTemparatureDescription = "PreheatTemparatureDescription is -16 ";
	public static final String interPassTemparatureDesc = "inter pas temparature 34";	
	public static final String Removaloflineupclamp = "Removing-AB03";	
	public static final String TimeintervalBetweenPasses = "2-3 hours ";	
	public static final String Amountofwelders = "500-/$";
	public static final String weldingposition = "Bend";
	public static final String weldingdirection = "Right corner";
	public static final String jointgeometryandfitup = "intermittent welding";
	public static final String weldingtechnique = "intermittent welding";
	public static final String jointcompletionandpasssequence = "sequence-219";
	public static final String weldingprocesslist = "welding-34";
	public static final String currentandpolarity = "sequence-219";
	public static final String coatingtype = "Sugar coating";
	public static final String Alloytype = "Ceramic";
	
	public static final String Passorder = "Order-23BYT";
	public static final String Weldpass = "Intermittent padss-7UY67";
	public static final String Classificationgrade = "UpgradeV-038";
	public static final String Classificationname = "Classify_Visur";
	public static final String Heatnumber = "4";
	public static final String Diameter = "2";
	public static final String Amperageminimum = "2";
	public static final String Amperagemaximum = "3";
	public static final String Voltageminimum = "111v";
	public static final String Voltagemaximum = "220kv";
	public static final String Travelspeedminimum = "100";
	public static final String Travelspeedmaximum = "200";
	public static final String Heatinputminimum = "24";
	public static final String Heatinputmaximum = "40";
	public static final String feedspeedminimum = "27";
	public static final String feedspeedmaximum = "28";
	public static final String gascomposition = "2";
	public static final String gasflowrateminimum = "2";
	public static final String gasflowratemaximum = "2";
	public static final String ossilationrateminimum = "2";
	public static final String ossilationratemaximum = "2";
	public static final String contacttiptoworkdistanceminimum = "2";
	public static final String contacttiptoworkdistancemaximum = "2";
	public static final String timeintervalsincepreviouspassminiummum = "2";
	public static final String timeintervalsincepreviouspassmaximum = "2";
	public static final String preheattemperatureminimum = "2";
	public static final String preheattemperaturemaximum = "2";
	
	public static final String Automation_procedure = "Automation_Procedures";
	public static final String FileName = "11Automation_File";
	public static final String FolderName = "1Automation_Folder";
	public static final String Rename    = "1Automation_Qa_Folder";
	public static final String RenameFile    = "11Automation_Qa_File";
	public static final String projecName = "MR GAT";
	public static final String Uploadedfilename = "Automation";
	
}

