
package Utils;

public class Constant {
	
	// EM- People
	
	public static final String Automation_Company = "Automation_Performance_Company";
	public static final String Automation_Person = "Automation_Performance _Person";
	
	public static final String EM_People = "EM_People&Companies";
	public static final String Company = "Company";
	public static final String person = "Person";
	public static final String config = "Config";
	
	// BI
	
	public static final String BusinessIntelligence = "Business Intelligence";
	
	public static final String bidashboard = "Dashboard";
	public static final String component = "Component Ratio DataSource";
	
	public static final String Integrity_Dig_Sample = "Integrity Dig Sample"; 
	public static final String Lem_Dashboard = "Lem Dashboard";
	public static final String Mainline_Controls = "Mainline Controls";
	public static final String Mainline_Dashboard = "Mainline Dashboard";
	public static final String Mainline_Plan_V_Actuals = "Mainline Plan V Actuals";
	public static final String Mainline_UPIs = "Mainline UPIs";
	public static final String Millwright_Example = "Millwright Example";
	public static final String Physical_Item_Spool = "Physical Item - Spool";
	public static final String Physical_Item_Weld = "Physical Item - Weld";
	public static final String Pile_Capping_Example = "Pile Capping Example";
	public static final String Sample_Sales = "Sample Sales";
	public static final String Tagged_Item_Concrete = "Tagged Item - Concrete";
	
	public static final String Equipment = "Equipment";
	public static final String Equipment_Category = "Equipment - Category";
	public static final String Equipment_Category1 = "Equipment Category";
	public static final String Inspection = "Inspection";
	public static final String Labor = "Labor";
	public static final String Labor_Category = "Labor - Category"; 
	public static final String Labor_Category1 = "Labor Category";
	public static final String Material = "Material";
	public static final String Production = "Production";
	public static final String Project = "Project";
	public static final String Welding = "Welding";
	public static final String Welding_NDT = "Welding - NDT";
}

