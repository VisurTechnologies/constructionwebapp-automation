package BI_Objects;

import java.util.List;
import java.util.Optional;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utils.BaseDriver;
import Utils.WebDriverCommonLib;

public class BI_Dashboard_Objects extends BaseDriver{
	
	public  BI_Dashboard_Objects(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	Actions actions = new Actions(driver);
	WebDriverWait wait = new WebDriverWait(driver, 60);
	WebDriverCommonLib wdc = new WebDriverCommonLib(driver);
	
	public By loadedcharts = By.xpath("//*[@class='mainWrapper']//*[@class='widgetContent']//canvas");

	
	public void chartloadtime() throws Exception {
		driver.switchTo().frame(0);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(loadedcharts)));
		wdc.imlicitlywaitfor_80();
		System.out.println("graph waiting time over");
		
		List<WebElement> chartcanvas = driver.findElements(loadedcharts);
		
		actions.click(chartcanvas.get(0)).build().perform();
		int i = 0;
		for (WebElement e : chartcanvas) {
			if (chartcanvas.get(i).isDisplayed()) {
				System.out.println("graph are loaded in dashboard");
			} else {
				System.out.println("graph are not loaded in dashboard");
			}
			i++;
		}
		driver.switchTo().defaultContent();
	}
	
//	public void enableThirdPartyCookies(WebDriver driver) throws Exception{
//		List list = driver.findElements(By.id("cookie-controls-toggle"));
//        Optional<WebElement> selectedCookieControlsToggle = driver.findElements(By.id("cookie-controls-toggle")).stream()
//                .filter(x -> x.getAttribute("checked") != null).findFirst();
//        Optional.ofNullable(selectedCookieControlsToggle).get().get().click();
//	}

}
