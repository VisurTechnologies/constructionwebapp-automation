package Entity_Management_Scripts;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.StopWatch;
import org.testng.annotations.Test;

import DB_Connection.DB_connection;
import Entity_Management_Objects.EntityManagement_Objects;
import SearchEntities.Searchentitiesobjects;
import Utils.Constant;
import Utils.LoginSetup_EM;

public class EM_Person_Load extends LoginSetup_EM{
	
	@Test
	public void EM_Person_Form_Load() throws Exception{
		Searchentitiesobjects search = new Searchentitiesobjects(driver);
		DB_connection dbconnection = new DB_connection();
		EntityManagement_Objects emcommon = new EntityManagement_Objects(driver);
		WebDriverWait wait = new WebDriverWait(driver, 60);
		StopWatch watch = new StopWatch();
		
		String method = Thread.currentThread().getStackTrace()[1].getMethodName();
		
		emcommon.clickOnLeftTree_PeoplAndCompanies();
		dbconnection.establishconnection();
		search.SearchEntity_People(Constant.Automation_Person);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(emcommon.personaccordion)));
		watch.start();
		emcommon.Checkpersondataload();
		watch.stop();
		
		System.out.println("Total execution time using StopWatch in millis for " +method +" is " + watch.getTotalTimeSeconds());

		double timetaken = watch.getTotalTimeSeconds();
		System.out.println(timetaken);
		dbconnection.insertintodb(method, Constant.EM_People, timetaken, Constant.Automation_Person,Constant.person, Constant.config);
	}
}
