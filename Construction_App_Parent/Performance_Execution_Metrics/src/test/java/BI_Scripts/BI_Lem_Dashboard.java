package BI_Scripts;

import org.springframework.util.StopWatch;
import org.testng.annotations.Test;

import BI_Objects.BI_Dashboard_Objects;
import DB_Connection.DB_connection;
import SearchEntities.Searchentitiesobjects;
import Utils.Constant;
import Utils.LoginSetup_BI;

public class BI_Lem_Dashboard extends LoginSetup_BI{
	
	@Test
	public void Lem_Dashboard() throws Exception{
		Searchentitiesobjects search = new Searchentitiesobjects(driver);
		DB_connection dbconnection = new DB_connection();
		BI_Dashboard_Objects bicommon = new BI_Dashboard_Objects(driver);
		StopWatch watch = new StopWatch();
		String method = Thread.currentThread().getStackTrace()[1].getMethodName();
		
		dbconnection.establishconnection();
		search.SearchEntity_BI(Constant.Lem_Dashboard);
		
		watch.start();
		bicommon.chartloadtime();
		watch.stop();
		System.out.println("Total execution time using StopWatch in millis for " +method +" is " + watch.getTotalTimeSeconds());

		double timetaken = watch.getTotalTimeSeconds();
		dbconnection.insertintodb(method, Constant.BusinessIntelligence, timetaken, Constant.Lem_Dashboard,Constant.bidashboard, Constant.Lem_Dashboard);
	}

}
