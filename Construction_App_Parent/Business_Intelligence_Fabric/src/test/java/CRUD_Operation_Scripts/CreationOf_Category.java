package CRUD_Operation_Scripts;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.testng.annotations.Test;

import BusinessIntelligenceCommonObjects.BusinessIntelligenceCommonObjects;
import CRUD_Operation_Objects.CRUD_Operation_CommonObjects;
import Utils.Constant;
import Utils.LoginSetup;

public class CreationOf_Category extends LoginSetup{
	@Test
	public void creationOfCategory() {
		System.out.println("Creation Of Category-------Execution Started");
		BusinessIntelligenceCommonObjects businessIntelligenceCommonObjects=new BusinessIntelligenceCommonObjects(driver);
		CRUD_Operation_CommonObjects CRUD_Operation_CommonObjects=new CRUD_Operation_CommonObjects(driver);
		
		LocalDate localDate = LocalDate.now();
		String s1 = DateTimeFormatter.ofPattern("yyy/MM/dd").format(localDate);
		
		businessIntelligenceCommonObjects.ClickPlusButton();
		CRUD_Operation_CommonObjects.selectEntityType_Category();
		CRUD_Operation_CommonObjects.selectParentEntity(Constant.parentEntity);
		CRUD_Operation_CommonObjects.enterEntityName(s1+Constant.creationCategory_EntityName);

		
	}

}
