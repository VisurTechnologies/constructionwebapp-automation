package CRUD_Operation_Objects;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utils.BaseDriver;
import Utils.WebDriverCommonLib;

public class CRUD_Operation_CommonObjects extends BaseDriver{

	public CRUD_Operation_CommonObjects(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	Actions actions = new Actions(driver);
	WebDriverWait wait = new WebDriverWait(driver, 60);
	WebDriverCommonLib wdc = new WebDriverCommonLib(driver);
	JavascriptExecutor je = (JavascriptExecutor) driver;
	
	public By entityTypes=By.xpath("//*[text()='Entity Type']/parent::div//*[@role='combobox']");
	public By ParentEntity=By.xpath("//*[text()='Parent Entity']/parent::div//*[@role='combobox']");
	public By EntityName=By.xpath("//*[@placeholder='Entity Name']");



	
	
	public void selectEntityType_Category() {
		wdc.waitForPageLoad();
		actions.click(driver.findElement(entityTypes)).build().perform();
		wdc.waitForPageLoad();
		List<WebElement> list=driver.findElements(By.xpath("//*[@role='option']//span"));
		wdc.waitForExpectedAllElements(list);
		for (WebElement e:list) {
			System.out.println(e.getText());
			if (e.getText().equalsIgnoreCase("Category")) {
				wdc.waitForPageLoad();
				e.click();
				wdc.waitForPageLoad();
				break;
			}
		}
	}
	
	public void selectParentEntity(String parentEntityName) {
		wdc.waitForPageLoad();
		actions.click(driver.findElement(ParentEntity)).build().perform();
		wdc.waitForPageLoad();
		List<WebElement> list=driver.findElements(By.xpath("//*[@role='option']//span"));
		wdc.waitForExpectedAllElements(list);
		for (WebElement e:list) {
			System.out.println(e.getText());
			if (e.getText().equalsIgnoreCase(parentEntityName)) {
				wdc.waitForPageLoad();
				e.click();
				wdc.waitForPageLoad();
				break;
			}
			scrollDown_ToElement(e);
		}
	}
	public void enterEntityName(String s) {
		wdc.waitForPageLoad();
		actions.click(driver.findElement(EntityName)).sendKeys(s).build().perform();
		wdc.waitForPageLoad();
	}

	public void scrollDown_ToElement(WebElement element) {
		try {
			je.executeScript("arguments[0].scrollIntoView(true);",element);
		} catch (Exception e) {
		}
		
	}


}
